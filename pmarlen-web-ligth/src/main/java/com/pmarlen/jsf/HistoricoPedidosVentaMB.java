package com.pmarlen.jsf;

import com.pmarlen.backend.dao.ClienteDAO;
import com.pmarlen.backend.dao.DAOException;
import com.pmarlen.backend.dao.FormaDePagoDAO;
import com.pmarlen.backend.dao.MetodoDePagoDAO;
import com.pmarlen.backend.dao.PedidoVentaDAO;
import com.pmarlen.backend.dao.ProductoDAO;
import com.pmarlen.backend.model.Cliente;
import com.pmarlen.backend.model.FormaDePago;
import com.pmarlen.backend.model.MetodoDePago;
import com.pmarlen.backend.model.PedidoVenta;
import com.pmarlen.backend.model.PedidoVentaDetalle;
import com.pmarlen.backend.model.Producto;
import com.pmarlen.backend.model.quickviews.PedidoVentaDetalleQuickView;
import com.pmarlen.backend.model.quickviews.PedidoVentaQuickView;
import com.pmarlen.model.Constants;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.primefaces.event.ReorderEvent;

@ManagedBean(name="historicoPedidosVentaMB")
@SessionScoped
public class HistoricoPedidosVentaMB  implements Serializable{
	private static Logger logger = Logger.getLogger("historicoPedidosVentaMB");
	
	@ManagedProperty(value = "#{editarPedidoVentaMB}")
	private EditarPedidoVentaMB editarPedidoVentaMB;	
	
	ArrayList<PedidoVentaQuickView> pedidosVentas;
	private int viewRows;
	
	@PostConstruct
	public void init(){
		logger.info("->init:");
		try {
			pedidosVentas = PedidoVentaDAO.getInstance().findAllHistorico();
			if(pedidosVentas != null){
				logger.config("pedidosVentas.size()="+pedidosVentas.size());
			}
		}catch(DAOException de){
			logger.severe(de.getMessage());
		}
		viewRows = 25;
	}
	
	public void refrescar(){
		logger.info("->refrescar:");
		try {
			pedidosVentas = PedidoVentaDAO.getInstance().findAllHistorico();
			if(pedidosVentas != null){
				logger.config("pedidosVentas.size()="+pedidosVentas.size());
			}
		}catch(DAOException de){
			logger.severe(de.getMessage());
		}
	}

	public void setEditarPedidoVentaMB(EditarPedidoVentaMB editarPedidoVentaMB) {
		this.editarPedidoVentaMB = editarPedidoVentaMB;
	}	

	public ArrayList<PedidoVentaQuickView> getPedidosVentas() {
		logger.config("->getPedidosVentas");
		return pedidosVentas;
	}
	
	public String editarPedido(int pedidoVentaId){
		logger.config("->editarPedido:pedidoVentaId="+pedidoVentaId);
		editarPedidoVentaMB.editar(pedidoVentaId);
		return "/pages/editarPedidoVenta";
	}
	
	public void onEditarPedido(int pedidoVentaId){
		logger.config("->editarPedido:pedidoVentaId="+pedidoVentaId);
		editarPedidoVentaMB.editar(pedidoVentaId);
	}
	
	public int getSizeList(){
		logger.fine("->getSizeList()");
		return getPedidosVentas().size();
	}

	public int getViewRows() {
		logger.fine("->getViewRows()");
		return viewRows;
	}

	public void setViewRows(int viewRows) {
		logger.fine("->setViewRows("+viewRows+")");
		this.viewRows = viewRows;
	}
	public String getImporteMoneda(double f){
		return Constants.getImporteMoneda(f);
	}

}
