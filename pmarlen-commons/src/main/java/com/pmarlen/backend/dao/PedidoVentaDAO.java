/**
 * PedidoVentaDAO
 *
 * Created 2014/12/30 17:09
 *
 * @author tracktopell :: DAO Builder http://www.tracktopell.com.mx
 */
package com.pmarlen.backend.dao;

import com.pmarlen.backend.model.*;
import com.pmarlen.backend.model.quickviews.PedidoVentaDetalleQuickView;
import com.pmarlen.backend.model.quickviews.PedidoVentaEstadoQuickView;
import com.pmarlen.backend.model.quickviews.PedidoVentaQuickView;
import com.pmarlen.model.Constants;
import com.tracktopell.jdbc.DataSourceFacade;

import java.io.ByteArrayInputStream;

import java.sql.Blob;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.Level;

/**
 * Class for PedidoVentaDAO of Table PEDIDO_VENTA.
 *
 * @author Tracktopell::jpa-builder @see
 * https://github.com/tracktopell/UtilProjects/tree/master/jpa-builder
 * @date 2014/12/30 17:09
 */
public class PedidoVentaDAO {

	private final static Logger logger = Logger.getLogger(PedidoVentaDAO.class.getName());

	/**
	 * Datasource for table PEDIDO_VENTA simple CRUD operations. x common
	 * paramenter.
	 */
	private static PedidoVentaDAO instance;

	private PedidoVentaDAO() {
		logger.log(Level.SEVERE, "--> created PedidoVentaDAO.");
	}

	public static PedidoVentaDAO getInstance() {
		if (instance == null) {
			instance = new PedidoVentaDAO();
		}
		return instance;
	}

	private Connection getConnection() {
		return DataSourceFacade.getStrategy().getConnection();
	}

	private Connection getConnectionCommiteable() {
		return DataSourceFacade.getStrategy().getConnectionCommiteable();
	}

	public PedidoVentaQuickView findBy(PedidoVenta p) throws DAOException, EntityNotFoundException {
		PedidoVentaQuickView x = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		PreparedStatement psPVE = null;
		ResultSet rsPVE = null;

		Connection conn = null;
		try {
			conn = getConnection();
			ps = conn.prepareStatement(
					"SELECT	PV.ID,PV.SUCURSAL_ID,PV.ESTADO_ID,PV.FECHA_CREO,PV.USUARIO_EMAIL_CREO,PV.CLIENTE_ID,PV.FORMA_DE_PAGO_ID,PV.METODO_DE_PAGO_ID,PV.FACTORIVA,PV.COMENTARIOS,PV.CFD_VENTA_ID,PV.NUMERO_TICKET,PV.CAJA,PV.IMPORTE_RECIBIDO,PV.APROBACION_VISA_MASTERCARD,PV.PORCENTAJE_DESCUENTO_CALCULADO,PV.PORCENTAJE_DESCUENTO_EXTRA,PV.CONDICIONES_DE_PAGO,PV.NUM_DE_CUENTA,\n"
					+ "CFD.ID AS CFD_VENTA_ID,\n"
					+ "S.NOMBRE AS SUCURSAL_NOMBRE,\n"
					+ "E.DESCRIPCION AS E_DESCRIPCION,\n"
					+ "U.NOMBRE_COMPLETO AS U_NOMBRE_COMPLETO,\n"
					+ "C.RFC AS C_RFC,\n"
					+ "C.RAZON_SOCIAL AS C_RAZON_SOCIAL,\n"
					+ "C.NOMBRE_ESTABLECIMIENTO AS C_NOMBRE_ESTABLECIMIENTO,\n"
					+ "FP.DESCRIPCION AS FP_DESCRIPCION,\n"
					+ "MP.DESCRIPCION AS MP_DESCRIPCION,\n"
					+ "CFD.NUM_CFD AS CFD_NUM_CFD,\n"
					+ "COUNT(1) NUM_ELEMENTOS, \n"
					+ "SUM(PVD.CANTIDAD * PVD.PRECIO_VENTA) AS IMPORTE_BRUTO\n"
					+ "FROM      PEDIDO_VENTA_DETALLE PVD,\n"
					+ "          PEDIDO_VENTA         PV\n"
					+ "LEFT JOIN CFD            CFD ON PV.CFD_VENTA_ID      = CFD.ID\n"
					+ "LEFT JOIN SUCURSAL       S   ON PV.SUCURSAL_ID       = S.ID\n"
					+ "LEFT JOIN ESTADO         E   ON PV.ESTADO_ID         = E.ID\n"
					+ "LEFT JOIN USUARIO        U   ON PV.USUARIO_EMAIL_CREO= U.EMAIL\n"
					+ "LEFT JOIN CLIENTE        C   ON PV.CLIENTE_ID        = C.ID\n"
					+ "LEFT JOIN FORMA_DE_PAGO  FP  ON PV.FORMA_DE_PAGO_ID  = FP.ID\n"
					+ "LEFT JOIN METODO_DE_PAGO MP  ON PV.METODO_DE_PAGO_ID = MP.ID\n"
					+ "WHERE     1=1\n"
					+ "AND       PV.ID = ?\n"
					+ "AND       PV.ID        = PVD.PEDIDO_VENTA_ID\n"
					+ "GROUP BY  PVD.PEDIDO_VENTA_ID\n"
					+ "ORDER BY  PV.FECHA_CREO DESC");

			ps.setInt(1, p.getId());

			rs = ps.executeQuery();
			while (rs.next()) {
				x = new PedidoVentaQuickView();
				x.setId((Integer) rs.getObject("ID"));
				x.setSucursalId((Integer) rs.getObject("SUCURSAL_ID"));
				x.setEstadoId((Integer) rs.getObject("ESTADO_ID"));
				x.setFechaCreo((Timestamp) rs.getObject("FECHA_CREO"));
				x.setUsuarioEmailCreo((String) rs.getObject("USUARIO_EMAIL_CREO"));
				x.setClienteId((Integer) rs.getObject("CLIENTE_ID"));
				x.setFormaDePagoId((Integer) rs.getObject("FORMA_DE_PAGO_ID"));
				x.setMetodoDePagoId((Integer) rs.getObject("METODO_DE_PAGO_ID"));
				x.setFactoriva((Double) rs.getObject("FACTORIVA"));
				x.setComentarios((String) rs.getObject("COMENTARIOS"));
				x.setCfdVentaId((Integer) rs.getObject("CFD_VENTA_ID"));
				x.setNumeroTicket((String) rs.getObject("NUMERO_TICKET"));
				x.setCaja((Integer) rs.getObject("CAJA"));
				x.setImporteRecibido((Double) rs.getObject("IMPORTE_RECIBIDO"));
				x.setAprobacionVisaMastercard((String) rs.getObject("APROBACION_VISA_MASTERCARD"));
				x.setPorcentajeDescuentoCalculado((Integer) rs.getObject("PORCENTAJE_DESCUENTO_CALCULADO"));
				x.setPorcentajeDescuentoExtra((Integer) rs.getObject("PORCENTAJE_DESCUENTO_EXTRA"));
				x.setCondicionesDePago((String) rs.getObject("CONDICIONES_DE_PAGO"));
				x.setNumDeCuenta((String) rs.getObject("NUM_DE_CUENTA"));

				x.setSucursalNombre((String) rs.getObject("SUCURSAL_NOMBRE"));
				x.setEstadoDescripcion((String) rs.getObject("E_DESCRIPCION"));
				x.setUsuarioNombreCompleto((String) rs.getObject("U_NOMBRE_COMPLETO"));
				x.setClienteRFC((String) rs.getObject("C_RFC"));
				x.setClienteRazonSocial((String) rs.getObject("C_RAZON_SOCIAL"));
				x.setClienteNombreEstablecimiento((String) rs.getObject("C_NOMBRE_ESTABLECIMIENTO"));
				x.setMetodoDePagoDescripcion((String) rs.getObject("MP_DESCRIPCION"));
				x.setFormaDePagoDescripcion((String) rs.getObject("FP_DESCRIPCION"));
				x.setCdfNumCFD((String) rs.getObject("CFD_NUM_CFD"));

				x.setNumElementos(rs.getInt("NUM_ELEMENTOS"));
				x.setImporteBruto(rs.getDouble("IMPORTE_BRUTO"));

				x.setImporteNoGravado(x.getImporteBruto() / (1.0 + x.getFactoriva()));
				x.setImporteIVA(x.getImporteBruto() - x.getImporteNoGravado());
				x.setImporteDescuento(0.0);
				x.setImporteTotal(x.getImporteBruto() - x.getImporteDescuento());
			}

			ArrayList<PedidoVentaEstadoQuickView> pveList = new ArrayList<PedidoVentaEstadoQuickView>();
			/*
			 SELECT PVE.ID,PVE.PEDIDO_VENTA_ID,PVE.ESTADO_ID,E.DESCRIPCION,PVE.FECHA,PVE.USUARIO_EMAIL,U.NOMBRE_COMPLETO,PVE.COMENTARIOS 
			 FROM   PEDIDO_VENTA_ESTADO PVE,ESTADO E,USUARIO U
			 WHERE  1=1
			 AND    PVE.ESTADO_ID=E.ID
			 AND    PVE.USUARIO_EMAIL = U.EMAIL
			 AND    PVE.PEDIDO_VENTA_ID=2548
			 ORDER BY PVE.FECHA;
			 */

			psPVE = conn.prepareStatement(
					"SELECT PVE.ID,PVE.PEDIDO_VENTA_ID,PVE.ESTADO_ID,E.DESCRIPCION,PVE.FECHA,PVE.USUARIO_EMAIL,U.NOMBRE_COMPLETO,PVE.COMENTARIOS \n"
					+ "FROM   PEDIDO_VENTA_ESTADO PVE,ESTADO E,USUARIO U\n"
					+ "WHERE  1=1\n"
					+ "AND    PVE.ESTADO_ID=E.ID\n"
					+ "AND    PVE.USUARIO_EMAIL = U.EMAIL\n"
					+ "AND    PVE.PEDIDO_VENTA_ID=?");
			psPVE.setInt(1, p.getId());

			rsPVE = psPVE.executeQuery();
			while (rsPVE.next()) {
				PedidoVentaEstadoQuickView z = new PedidoVentaEstadoQuickView();

				z.setId(rsPVE.getInt("ID"));
				z.setPedidoVentaId(rsPVE.getInt("PEDIDO_VENTA_ID"));
				z.setEstadoId(rsPVE.getInt("ESTADO_ID"));
				z.setEstadoDescripcion(rsPVE.getString("DESCRIPCION"));
				z.setFecha(rsPVE.getTimestamp("FECHA"));
				z.setUsuarioEmail(rsPVE.getString("USUARIO_EMAIL"));
				z.setUsuarioNombreCompleto(rsPVE.getString("NOMBRE_COMPLETO"));
				z.setComentarios(rsPVE.getString("COMENTARIOS"));

				pveList.add(z);
			}
			x.setPveList(pveList);

		} catch (SQLException ex) {
			logger.log(Level.SEVERE, "SQLException:", ex);
			throw new DAOException("InQuery:" + ex.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
					ps.close();

					rsPVE.close();
					psPVE.close();

					conn.close();
				} catch (SQLException ex) {
					logger.log(Level.SEVERE, "clossing, SQLException:" + ex.getMessage());
					throw new DAOException("Closing:" + ex.getMessage());
				}
			}
		}
		return x;
	}

	public ArrayList<PedidoVentaDetalleQuickView> findAllPVDByPedidoVenta(int pedidoVentaId) throws DAOException {
		ArrayList<PedidoVentaDetalleQuickView> r = new ArrayList<PedidoVentaDetalleQuickView>();

		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		try {
			conn = getConnection();

			ps = conn.prepareStatement(
					"SELECT P.CODIGO_BARRAS,P.NOMBRE,P.PRESENTACION,P.INDUSTRIA,P.MARCA,P.LINEA,P.CONTENIDO,P.UNIDAD_MEDIDA,AP.PRECIO,AP.CANTIDAD,PVD.ID AS PVD_ID,A.ID AS ALMACEN_ID,A.TIPO_ALMACEN,PVD.CANTIDAD AS CANTIDAD_PVD,PVD.PRECIO_VENTA\n"
					+ "FROM   PEDIDO_VENTA PV,\n"
					+ "       PEDIDO_VENTA_DETALLE PVD,\n"
					+ "       PRODUCTO P,\n"
					+ "       ALMACEN_PRODUCTO AP,\n"
					+ "       ALMACEN A\n"
					+ "WHERE  1=1\n"
					+ "AND    PV.ID                      = ?\n"
					+ "AND    PV.ID                      = PVD.PEDIDO_VENTA_ID\n"
					+ "AND    PVD.PRODUCTO_CODIGO_BARRAS = P.CODIGO_BARRAS\n"
					+ "AND    PVD.PRODUCTO_CODIGO_BARRAS = AP.PRODUCTO_CODIGO_BARRAS\n"
					+ "AND    AP.ALMACEN_ID=A.ID\n"
					+ "AND    A.ID=PVD.ALMACEN_ID");

			ps.setInt(1, pedidoVentaId);

			rs = ps.executeQuery();
			while (rs.next()) {
				PedidoVentaDetalleQuickView x = new PedidoVentaDetalleQuickView();
				x.setPedidoVentaId(pedidoVentaId);
				x.setProductoCodigoBarras(rs.getString("CODIGO_BARRAS"));
				x.setProductoNombre(rs.getString("NOMBRE"));
				x.setProductoPresentacion(rs.getString("PRESENTACION"));
				x.setProductoIndustria(rs.getString("INDUSTRIA"));
				x.setProductoMarca(rs.getString("MARCA"));
				x.setProductoLinea(rs.getString("LINEA"));
				x.setProductoContenido(rs.getString("CONTENIDO"));
				x.setProductoUnidadMedida(rs.getString("UNIDAD_MEDIDA"));
				x.setPrecioVenta(rs.getDouble("PRECIO_VENTA"));
				x.setCantidad(rs.getInt("CANTIDAD_PVD"));
				x.setId(rs.getInt("PVD_ID"));
				x.setAlmacenId(rs.getInt("ALMACEN_ID"));
				x.setApTipoAlmacen(rs.getInt("TIPO_ALMACEN"));
				x.setApCantidad(rs.getInt("CANTIDAD"));

				r.add(x);
			}

		} catch (SQLException ex) {
			logger.log(Level.SEVERE, "SQLException:", ex);
			throw new DAOException("InQuery:" + ex.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
					ps.close();
					conn.close();
				} catch (SQLException ex) {
					logger.log(Level.SEVERE, "clossing, SQLException:" + ex.getMessage());
					throw new DAOException("Closing:" + ex.getMessage());
				}
			}
		}
		return r;
	}
	/*
	
	 SELECT ID FROM PEDIDO_VENTA 
	 WHERE ESTADO_IDIN (1,2,4);
	
	 SELECT PVD.PEDIDO_VENTA_ID,PVD.PRODUCTO_CODIGO_BARRAS,PVD.ALMACEN_ID,PVD.CANTIDAD 
	 FROM   PEDIDO_VENTA_DETALLE PVD,PEDIDO_VENTA PV
	 WHERE  1 = 1 
	 AND    PVD.PEDIDO_VENTA_ID = PV.ID 
	 AND    PV.ESTADO_ID IN (1,2,4)
	 AND    PVD.PRODUCTO_CODIGO_BARRAS IN ('4005800631702','4005808200108','6375','7503016269803','7503016269797','8410190583290','8410190583292')
	 ORDER BY PVD.PRODUCTO_CODIGO_BARRAS,PVD.ALMACEN_ID,PVD.ID;

	
	 SELECT PVD.PEDIDO_VENTA_ID,AP.CANTIDAD,PVD.PRODUCTO_CODIGO_BARRAS,PVD.ALMACEN_ID,PVD.CANTIDAD
	 FROM   PEDIDO_VENTA_DETALLE PVD,PEDIDO_VENTA PV,ALMACEN_PRODUCTO AP
	 WHERE  1 = 1 
	 AND    PVD.PRODUCTO_CODIGO_BARRAS = AP.PRODUCTO_CODIGO_BARRAS
	 AND    PVD.ALMACEN_ID = AP.ALMACEN_ID
	 AND    PVD.PEDIDO_VENTA_ID = PV.ID 
	 AND    PV.ESTADO_ID IN (1,2,4)
	 AND    PV.ID <> 2194
	 AND    PVD.PRODUCTO_CODIGO_BARRAS IN ('4005800631702','4005808200108','6375','7503016269803','7503016269797','8410190583290','8410190583292')
	 ORDER BY PVD.PRODUCTO_CODIGO_BARRAS,PVD.ALMACEN_ID,PVD.ID;
	
	 SELECT AP.CANTIDAD,PVD.PRODUCTO_CODIGO_BARRAS,PVD.ALMACEN_ID,SUM(PVD.CANTIDAD) TOT_CANTIDAD 
	 FROM   PEDIDO_VENTA_DETALLE PVD,PEDIDO_VENTA PV,ALMACEN_PRODUCTO AP
	 WHERE  1 = 1 
	 AND    PVD.PRODUCTO_CODIGO_BARRAS = AP.PRODUCTO_CODIGO_BARRAS
	 AND    PVD.ALMACEN_ID = AP.ALMACEN_ID
	 AND    PVD.PEDIDO_VENTA_ID = PV.ID 
	 AND    PV.ESTADO_ID IN (1,2,4)
	 AND    PV.ID <> 2547
	 AND    PVD.PRODUCTO_CODIGO_BARRAS IN ('7501438302009','7509546047591','7501022183861','7509546050966','7506078932876','7501022103203','7502244766610','4005808944200','7502244764517','7502221183881','7501737420053','7509546055725')
	 GROUP BY PVD.PRODUCTO_CODIGO_BARRAS,PVD.ALMACEN_ID
	 ORDER BY PVD.PRODUCTO_CODIGO_BARRAS,PVD.ALMACEN_ID,PVD.ID;
	
	 */

	public void actualizaCantidadPendienteParaOtrosPV(ArrayList<PedidoVentaDetalleQuickView> pvdList) throws DAOException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		try {
			conn = getConnection();

			HashSet<String> codigos = new HashSet<String>();
			int pedidoVentaId = 0;
			for (PedidoVentaDetalleQuickView pvd : pvdList) {
				pedidoVentaId = pvd.getPedidoVentaId();
				codigos.add(pvd.getProductoCodigoBarras());
			}

			int ncb = 0;
			StringBuffer sbCB = new StringBuffer();
			String codigosBuscar = null;
			for (String cb : codigos) {
				if (ncb == 0) {
					sbCB.append("'");
					sbCB.append(cb);
					sbCB.append("'");
				} else {
					sbCB.append(",'");
					sbCB.append(cb);
					sbCB.append("'");
				}
				ncb++;
			}
			codigosBuscar = sbCB.toString();

			logger.info("pedidoVentaId=" + pedidoVentaId + ", codigosBuscar= ->" + codigosBuscar + "<-");

			ps = conn.prepareStatement(
					"SELECT AP.CANTIDAD,PVD.PRODUCTO_CODIGO_BARRAS,PVD.ALMACEN_ID,SUM(PVD.CANTIDAD) TOT_CANTIDAD \n"
					+ "FROM   PEDIDO_VENTA_DETALLE PVD,PEDIDO_VENTA PV,ALMACEN_PRODUCTO AP\n"
					+ "WHERE  1 = 1 \n"
					+ "AND    PVD.PRODUCTO_CODIGO_BARRAS = AP.PRODUCTO_CODIGO_BARRAS\n"
					+ "AND    PVD.ALMACEN_ID = AP.ALMACEN_ID\n"
					+ "AND    PVD.PEDIDO_VENTA_ID = PV.ID \n"
					+ "AND    PV.ESTADO_ID IN (1,2,4)\n"
					+ "AND    PV.ID <> ? \n"
					+ "AND    PVD.PRODUCTO_CODIGO_BARRAS IN ("
					+ codigosBuscar
					+ ")\n"
					+ "GROUP BY PVD.PRODUCTO_CODIGO_BARRAS,PVD.ALMACEN_ID\n"
					+ "ORDER BY PVD.PRODUCTO_CODIGO_BARRAS,PVD.ALMACEN_ID,PVD.ID");

			ps.setInt(1, pedidoVentaId);

			rs = ps.executeQuery();
			while (rs.next()) {
				int axi = rs.getInt("ALMACEN_ID");
				String cbxi = rs.getString("PRODUCTO_CODIGO_BARRAS");
				int tcxi = rs.getInt("TOT_CANTIDAD");
				int apcxi = rs.getInt("CANTIDAD");

				logger.info("Iteration:\t" + axi + "," + cbxi + ", " + tcxi + ", " + apcxi);

				for (PedidoVentaDetalleQuickView pvd : pvdList) {
					if (pvd.getProductoCodigoBarras().equals(cbxi) && pvd.getAlmacenId() == axi) {
						pvd.setCanPendteEnOtrosPV(tcxi);
						pvd.setApCantidad(apcxi);
						break;
					}
				}
			}

		} catch (SQLException ex) {
			logger.log(Level.SEVERE, "SQLException:", ex);
			throw new DAOException("InQuery:" + ex.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
					ps.close();
					conn.close();
				} catch (SQLException ex) {
					logger.log(Level.SEVERE, "findAll:clossing:", ex);
				}
			}
		}
	}

	public ArrayList<PedidoVenta> findAll() throws DAOException {
		ArrayList<PedidoVenta> r = new ArrayList<PedidoVenta>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		try {
			conn = getConnection();
			ps = conn.prepareStatement("SELECT ID,SUCURSAL_ID,ESTADO_ID,FECHA_CREO,USUARIO_EMAIL_CREO,CLIENTE_ID,FORMA_DE_PAGO_ID,METODO_DE_PAGO_ID,FACTORIVA,COMENTARIOS,CFD_VENTA_ID,NUMERO_TICKET,CAJA,IMPORTE_RECIBIDO,APROBACION_VISA_MASTERCARD,PORCENTAJE_DESCUENTO_CALCULADO,PORCENTAJE_DESCUENTO_EXTRA,CONDICIONES_DE_PAGO,NUM_DE_CUENTA FROM PEDIDO_VENTA");

			rs = ps.executeQuery();
			while (rs.next()) {
				PedidoVenta x = new PedidoVenta();
				x.setId((Integer) rs.getObject("ID"));
				x.setSucursalId((Integer) rs.getObject("SUCURSAL_ID"));
				x.setEstadoId((Integer) rs.getObject("ESTADO_ID"));
				x.setFechaCreo((Timestamp) rs.getObject("FECHA_CREO"));
				x.setUsuarioEmailCreo((String) rs.getObject("USUARIO_EMAIL_CREO"));
				x.setClienteId((Integer) rs.getObject("CLIENTE_ID"));
				x.setFormaDePagoId((Integer) rs.getObject("FORMA_DE_PAGO_ID"));
				x.setMetodoDePagoId((Integer) rs.getObject("METODO_DE_PAGO_ID"));
				x.setFactoriva((Double) rs.getObject("FACTORIVA"));
				x.setComentarios((String) rs.getObject("COMENTARIOS"));
				x.setCfdVentaId((Integer) rs.getObject("CFD_VENTA_ID"));
				x.setNumeroTicket((String) rs.getObject("NUMERO_TICKET"));
				x.setCaja((Integer) rs.getObject("CAJA"));
				x.setImporteRecibido((Double) rs.getObject("IMPORTE_RECIBIDO"));
				x.setAprobacionVisaMastercard((String) rs.getObject("APROBACION_VISA_MASTERCARD"));
				x.setPorcentajeDescuentoCalculado((Integer) rs.getObject("PORCENTAJE_DESCUENTO_CALCULADO"));
				x.setPorcentajeDescuentoExtra((Integer) rs.getObject("PORCENTAJE_DESCUENTO_EXTRA"));
				x.setCondicionesDePago((String) rs.getObject("CONDICIONES_DE_PAGO"));
				x.setNumDeCuenta((String) rs.getObject("NUM_DE_CUENTA"));
				r.add(x);
			}
		} catch (SQLException ex) {
			logger.log(Level.SEVERE, "SQLException:", ex);
			throw new DAOException("InQuery:" + ex.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
					ps.close();
					conn.close();
				} catch (SQLException ex) {
					logger.log(Level.SEVERE, "findAll:clossing:", ex);
				}
			}
		}
		return r;
	}

	;
	
    public ArrayList<PedidoVentaQuickView> findAllActive() throws DAOException {
		ArrayList<PedidoVentaQuickView> r = new ArrayList<PedidoVentaQuickView>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		try {
			conn = getConnection();

			ps = conn.prepareStatement(
					"SELECT	PV.ID,PV.SUCURSAL_ID,PV.ESTADO_ID,PV.FECHA_CREO,PV.USUARIO_EMAIL_CREO,PV.CLIENTE_ID,PV.FORMA_DE_PAGO_ID,PV.METODO_DE_PAGO_ID,PV.FACTORIVA,PV.COMENTARIOS,PV.CFD_VENTA_ID,PV.NUMERO_TICKET,PV.CAJA,PV.IMPORTE_RECIBIDO,PV.APROBACION_VISA_MASTERCARD,PV.PORCENTAJE_DESCUENTO_CALCULADO,PV.PORCENTAJE_DESCUENTO_EXTRA,PV.CONDICIONES_DE_PAGO,PV.NUM_DE_CUENTA,\n"
					+ "CFD.ID AS CFD_VENTA_ID,\n"
					+ "S.NOMBRE AS SUCURSAL_NOMBRE,\n"
					+ "E.DESCRIPCION AS E_DESCRIPCION,\n"
					+ "U.NOMBRE_COMPLETO AS U_NOMBRE_COMPLETO,\n"
					+ "C.RFC AS C_RFC,\n"
					+ "C.RAZON_SOCIAL AS C_RAZON_SOCIAL,\n"
					+ "C.NOMBRE_ESTABLECIMIENTO AS C_NOMBRE_ESTABLECIMIENTO,\n"
					+ "FP.DESCRIPCION AS FP_DESCRIPCION,\n"
					+ "MP.DESCRIPCION AS MP_DESCRIPCION,\n"
					+ "CFD.NUM_CFD AS CFD_NUM_CFD,\n"
					+ "COUNT(1) NUM_ELEMENTOS, \n"
					+ "SUM(PVD.CANTIDAD * PVD.PRECIO_VENTA) AS IMPORTE_BRUTO\n"
					+ "FROM      PEDIDO_VENTA_DETALLE PVD,\n"
					+ "          PEDIDO_VENTA         PV\n"
					+ "LEFT JOIN CFD            CFD ON PV.CFD_VENTA_ID      = CFD.ID\n"
					+ "LEFT JOIN SUCURSAL       S   ON PV.SUCURSAL_ID       = S.ID\n"
					+ "LEFT JOIN ESTADO         E   ON PV.ESTADO_ID         = E.ID\n"
					+ "LEFT JOIN USUARIO        U   ON PV.USUARIO_EMAIL_CREO= U.EMAIL\n"
					+ "LEFT JOIN CLIENTE        C   ON PV.CLIENTE_ID        = C.ID\n"
					+ "LEFT JOIN FORMA_DE_PAGO  FP  ON PV.FORMA_DE_PAGO_ID  = FP.ID\n"
					+ "LEFT JOIN METODO_DE_PAGO MP  ON PV.METODO_DE_PAGO_ID = MP.ID\n"
					+ "WHERE     1=1\n"
					+ "AND       PV.ESTADO_ID IN(1,2,4,8)\n"
					+ "AND       PV.ID        = PVD.PEDIDO_VENTA_ID\n"
					+ "GROUP BY  PVD.PEDIDO_VENTA_ID\n"
					+ "ORDER BY  PV.FECHA_CREO DESC");

			rs = ps.executeQuery();
			while (rs.next()) {
				PedidoVentaQuickView x = new PedidoVentaQuickView();
				x.setId((Integer) rs.getObject("ID"));
				x.setSucursalId((Integer) rs.getObject("SUCURSAL_ID"));
				x.setEstadoId((Integer) rs.getObject("ESTADO_ID"));
				x.setFechaCreo((Timestamp) rs.getObject("FECHA_CREO"));
				x.setUsuarioEmailCreo((String) rs.getObject("USUARIO_EMAIL_CREO"));
				x.setClienteId((Integer) rs.getObject("CLIENTE_ID"));
				x.setFormaDePagoId((Integer) rs.getObject("FORMA_DE_PAGO_ID"));
				x.setMetodoDePagoId((Integer) rs.getObject("METODO_DE_PAGO_ID"));
				x.setFactoriva((Double) rs.getObject("FACTORIVA"));
				x.setComentarios((String) rs.getObject("COMENTARIOS"));
				x.setCfdVentaId((Integer) rs.getObject("CFD_VENTA_ID"));
				x.setNumeroTicket((String) rs.getObject("NUMERO_TICKET"));
				x.setCaja((Integer) rs.getObject("CAJA"));
				x.setImporteRecibido((Double) rs.getObject("IMPORTE_RECIBIDO"));
				x.setAprobacionVisaMastercard((String) rs.getObject("APROBACION_VISA_MASTERCARD"));
				x.setPorcentajeDescuentoCalculado((Integer) rs.getObject("PORCENTAJE_DESCUENTO_CALCULADO"));
				x.setPorcentajeDescuentoExtra((Integer) rs.getObject("PORCENTAJE_DESCUENTO_EXTRA"));
				x.setCondicionesDePago((String) rs.getObject("CONDICIONES_DE_PAGO"));
				x.setNumDeCuenta((String) rs.getObject("NUM_DE_CUENTA"));

				x.setSucursalNombre((String) rs.getObject("SUCURSAL_NOMBRE"));
				x.setEstadoDescripcion((String) rs.getObject("E_DESCRIPCION"));
				x.setUsuarioNombreCompleto((String) rs.getObject("U_NOMBRE_COMPLETO"));
				x.setClienteRFC((String) rs.getObject("C_RFC"));
				x.setClienteRazonSocial((String) rs.getObject("C_RAZON_SOCIAL"));
				x.setClienteNombreEstablecimiento((String) rs.getObject("C_NOMBRE_ESTABLECIMIENTO"));
				x.setMetodoDePagoDescripcion((String) rs.getObject("MP_DESCRIPCION"));
				x.setFormaDePagoDescripcion((String) rs.getObject("FP_DESCRIPCION"));
				x.setCdfNumCFD((String) rs.getObject("CFD_NUM_CFD"));

				x.setNumElementos(rs.getInt("NUM_ELEMENTOS"));
				x.setImporteBruto(rs.getDouble("IMPORTE_BRUTO"));

				x.setImporteNoGravado(x.getImporteBruto() / (1.0 + x.getFactoriva()));
				x.setImporteIVA(x.getImporteBruto() - x.getImporteNoGravado());
				x.setImporteDescuento((x.getImporteBruto() * (x.getPorcentajeDescuentoCalculado()+x.getPorcentajeDescuentoExtra()))/100.0);
				x.setImporteTotal(x.getImporteBruto() - x.getImporteDescuento());

				r.add(x);
			}
		} catch (SQLException ex) {
			logger.log(Level.SEVERE, "SQLException:", ex);
			throw new DAOException("InQuery:" + ex.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
					ps.close();
					conn.close();
				} catch (SQLException ex) {
					logger.log(Level.SEVERE, "findAll:clossing:", ex);
				}
			}
		}
		return r;
	}

	;
	
    public ArrayList<PedidoVentaQuickView> findAllHistorico() throws DAOException {
		ArrayList<PedidoVentaQuickView> r = new ArrayList<PedidoVentaQuickView>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		try {
			conn = getConnection();

			ps = conn.prepareStatement(
					"SELECT	PV.ID,PV.SUCURSAL_ID,PV.ESTADO_ID,PV.FECHA_CREO,PV.USUARIO_EMAIL_CREO,PV.CLIENTE_ID,PV.FORMA_DE_PAGO_ID,PV.METODO_DE_PAGO_ID,PV.FACTORIVA,PV.COMENTARIOS,PV.CFD_VENTA_ID,PV.NUMERO_TICKET,PV.CAJA,PV.IMPORTE_RECIBIDO,PV.APROBACION_VISA_MASTERCARD,PV.PORCENTAJE_DESCUENTO_CALCULADO,PV.PORCENTAJE_DESCUENTO_EXTRA,PV.CONDICIONES_DE_PAGO,PV.NUM_DE_CUENTA,\n"
					+ "CFD.ID AS CFD_VENTA_ID,\n"
					+ "S.NOMBRE AS SUCURSAL_NOMBRE,\n"
					+ "E.DESCRIPCION AS E_DESCRIPCION,\n"
					+ "U.NOMBRE_COMPLETO AS U_NOMBRE_COMPLETO,\n"
					+ "C.RFC AS C_RFC,\n"
					+ "C.RAZON_SOCIAL AS C_RAZON_SOCIAL,\n"
					+ "C.NOMBRE_ESTABLECIMIENTO AS C_NOMBRE_ESTABLECIMIENTO,\n"
					+ "FP.DESCRIPCION AS FP_DESCRIPCION,\n"
					+ "MP.DESCRIPCION AS MP_DESCRIPCION,\n"
					+ "CFD.NUM_CFD AS CFD_NUM_CFD,\n"
					+ "COUNT(1) NUM_ELEMENTOS, \n"
					+ "SUM(PVD.CANTIDAD * PVD.PRECIO_VENTA) AS IMPORTE_BRUTO\n"
					+ "FROM      PEDIDO_VENTA_DETALLE PVD,\n"
					+ "          PEDIDO_VENTA         PV\n"
					+ "LEFT JOIN CFD            CFD ON PV.CFD_VENTA_ID      = CFD.ID\n"
					+ "LEFT JOIN SUCURSAL       S   ON PV.SUCURSAL_ID       = S.ID\n"
					+ "LEFT JOIN ESTADO         E   ON PV.ESTADO_ID         = E.ID\n"
					+ "LEFT JOIN USUARIO        U   ON PV.USUARIO_EMAIL_CREO= U.EMAIL\n"
					+ "LEFT JOIN CLIENTE        C   ON PV.CLIENTE_ID        = C.ID\n"
					+ "LEFT JOIN FORMA_DE_PAGO  FP  ON PV.FORMA_DE_PAGO_ID  = FP.ID\n"
					+ "LEFT JOIN METODO_DE_PAGO MP  ON PV.METODO_DE_PAGO_ID = MP.ID\n"
					+ "WHERE     1=1\n"
					+ "AND       PV.ESTADO_ID > 8\n"
					+ "AND       PV.ID        = PVD.PEDIDO_VENTA_ID\n"
					+ "GROUP BY  PVD.PEDIDO_VENTA_ID\n"
					+ "ORDER BY  PV.FECHA_CREO DESC");

			rs = ps.executeQuery();
			while (rs.next()) {
				PedidoVentaQuickView x = new PedidoVentaQuickView();
				x.setId((Integer) rs.getObject("ID"));
				x.setSucursalId((Integer) rs.getObject("SUCURSAL_ID"));
				x.setEstadoId((Integer) rs.getObject("ESTADO_ID"));
				x.setFechaCreo((Timestamp) rs.getObject("FECHA_CREO"));
				x.setUsuarioEmailCreo((String) rs.getObject("USUARIO_EMAIL_CREO"));
				x.setClienteId((Integer) rs.getObject("CLIENTE_ID"));
				x.setFormaDePagoId((Integer) rs.getObject("FORMA_DE_PAGO_ID"));
				x.setMetodoDePagoId((Integer) rs.getObject("METODO_DE_PAGO_ID"));
				x.setFactoriva((Double) rs.getObject("FACTORIVA"));
				x.setComentarios((String) rs.getObject("COMENTARIOS"));
				x.setCfdVentaId((Integer) rs.getObject("CFD_VENTA_ID"));
				x.setNumeroTicket((String) rs.getObject("NUMERO_TICKET"));
				x.setCaja((Integer) rs.getObject("CAJA"));
				x.setImporteRecibido((Double) rs.getObject("IMPORTE_RECIBIDO"));
				x.setAprobacionVisaMastercard((String) rs.getObject("APROBACION_VISA_MASTERCARD"));
				x.setPorcentajeDescuentoCalculado((Integer) rs.getObject("PORCENTAJE_DESCUENTO_CALCULADO"));
				x.setPorcentajeDescuentoExtra((Integer) rs.getObject("PORCENTAJE_DESCUENTO_EXTRA"));
				x.setCondicionesDePago((String) rs.getObject("CONDICIONES_DE_PAGO"));
				x.setNumDeCuenta((String) rs.getObject("NUM_DE_CUENTA"));

				x.setSucursalNombre((String) rs.getObject("SUCURSAL_NOMBRE"));
				x.setEstadoDescripcion((String) rs.getObject("E_DESCRIPCION"));
				x.setUsuarioNombreCompleto((String) rs.getObject("U_NOMBRE_COMPLETO"));
				x.setClienteRFC((String) rs.getObject("C_RFC"));
				x.setClienteRazonSocial((String) rs.getObject("C_RAZON_SOCIAL"));
				x.setClienteNombreEstablecimiento((String) rs.getObject("C_NOMBRE_ESTABLECIMIENTO"));
				x.setMetodoDePagoDescripcion((String) rs.getObject("MP_DESCRIPCION"));
				x.setFormaDePagoDescripcion((String) rs.getObject("FP_DESCRIPCION"));
				x.setCdfNumCFD((String) rs.getObject("CFD_NUM_CFD"));

				x.setNumElementos(rs.getInt("NUM_ELEMENTOS"));
				x.setImporteBruto(rs.getDouble("IMPORTE_BRUTO"));

				x.setImporteNoGravado(x.getImporteBruto() / (1.0 + x.getFactoriva()));
				x.setImporteIVA(x.getImporteBruto() - x.getImporteNoGravado());
				x.setImporteDescuento((x.getImporteBruto() * (x.getPorcentajeDescuentoCalculado()+x.getPorcentajeDescuentoExtra()))/100.0);
				x.setImporteTotal(x.getImporteBruto() - x.getImporteDescuento());

				r.add(x);
			}
		} catch (SQLException ex) {
			logger.log(Level.SEVERE, "SQLException:", ex);
			throw new DAOException("InQuery:" + ex.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
					ps.close();
					conn.close();
				} catch (SQLException ex) {
					logger.log(Level.SEVERE, "findAll:clossing:", ex);
				}
			}
		}
		return r;
	}

	;
	
    public int insert(PedidoVenta x, ArrayList<? extends PedidoVentaDetalle> pvdList) throws DAOException {
		PreparedStatement ps = null;
		PreparedStatement psPVE = null;
		PreparedStatement psPVD = null;
		int r = -1;
		Connection conn = null;
		try {
			conn = getConnectionCommiteable();

			Timestamp now = new Timestamp(System.currentTimeMillis());
			ps = conn.prepareStatement("INSERT INTO PEDIDO_VENTA(SUCURSAL_ID,ESTADO_ID,FECHA_CREO,USUARIO_EMAIL_CREO,CLIENTE_ID,FORMA_DE_PAGO_ID,METODO_DE_PAGO_ID,FACTORIVA,COMENTARIOS,CFD_VENTA_ID,NUMERO_TICKET,CAJA,IMPORTE_RECIBIDO,APROBACION_VISA_MASTERCARD,PORCENTAJE_DESCUENTO_CALCULADO,PORCENTAJE_DESCUENTO_EXTRA,CONDICIONES_DE_PAGO,NUM_DE_CUENTA) "
					+ " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
			int ci = 1;
			ps.setObject(ci++, x.getId());
			ps.setObject(ci++, x.getSucursalId());
			ps.setObject(ci++, x.getEstadoId());
			ps.setObject(ci++, now);
			ps.setObject(ci++, x.getUsuarioEmailCreo());
			ps.setObject(ci++, x.getClienteId());
			ps.setObject(ci++, x.getFormaDePagoId());
			ps.setObject(ci++, x.getMetodoDePagoId());
			ps.setObject(ci++, x.getFactoriva());
			ps.setObject(ci++, x.getComentarios());
			ps.setObject(ci++, x.getCfdVentaId());
			ps.setObject(ci++, x.getNumeroTicket());
			ps.setObject(ci++, x.getCaja());
			ps.setObject(ci++, x.getImporteRecibido());
			ps.setObject(ci++, x.getAprobacionVisaMastercard());
			ps.setObject(ci++, x.getPorcentajeDescuentoCalculado());
			ps.setObject(ci++, x.getPorcentajeDescuentoExtra());
			ps.setObject(ci++, x.getCondicionesDePago());
			ps.setObject(ci++, x.getNumDeCuenta());

			r = ps.executeUpdate();

			ResultSet rsk = ps.getGeneratedKeys();
			if (rsk != null) {
				while (rsk.next()) {
					x.setId(rsk.getInt(1));
				}
			}

			psPVD = conn.prepareStatement("INSERT INTO PEDIDO_VENTA_DETALLE(PEDIDO_VENTA_ID,PRODUCTO_CODIGO_BARRAS,ALMACEN_ID,CANTIDAD,PRECIO_VENTA) "
					+ " VALUES(?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
			int rPVD = 0;
			for (PedidoVentaDetalle pvd : pvdList) {
				int ciPVD = 1;

				psPVD.clearParameters();
				psPVD.clearWarnings();

				psPVD.setInt(ciPVD++, x.getId());
				if (pvd.getProductoCodigoBarras() == null) {
					psPVD.setObject(ciPVD++, null);
				} else {
					psPVD.setString(ciPVD++, pvd.getProductoCodigoBarras());
				}
				psPVD.setInt(ciPVD++, pvd.getAlmacenId());
				psPVD.setInt(ciPVD++, pvd.getCantidad());
				psPVD.setDouble(ciPVD++, pvd.getPrecioVenta());

				rPVD += psPVD.executeUpdate();
			}

			psPVE = conn.prepareStatement("INSERT INTO PEDIDO_VENTA_ESTADO(PEDIDO_VENTA_ID,ESTADO_ID,FECHA,USUARIO_EMAIL,COMENTARIOS) "
					+ " VALUES(?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
			int[] estados = new int[]{Constants.ESTADO_CAPTURADO, Constants.ESTADO_SINCRONIZADO};
			int ciPVE = 1;

			int rPVE = -1;
			for (int j = 0; j < 2; j++) {
				psPVE.clearParameters();
				ciPVE = 1;

				psPVE.setInt(ciPVE++, x.getId());
				psPVE.setInt(ciPVE++, estados[j]);
				psPVE.setTimestamp(ciPVE++, now);
				psPVE.setString(ciPVE++, x.getUsuarioEmailCreo());
				psPVE.setString(ciPVE++, "--NORMAL--");

				rPVE += psPVE.executeUpdate();
			}
			conn.commit();

		} catch (SQLException ex) {
			logger.log(Level.SEVERE, "SQLException:", ex);
			try {
				conn.rollback();
			} catch (SQLException exR) {
				logger.log(Level.SEVERE, "RollBack failed:", ex);
			}
			throw new DAOException("InUpdate:" + ex.getMessage());
		} finally {
			if (ps != null) {
				try {
					ps.close();
					conn.close();
				} catch (SQLException ex) {
					logger.log(Level.SEVERE, "clossing, SQLException:" + ex.getMessage());
					throw new DAOException("Closing:" + ex.getMessage());
				}
			}
		}
		return r;
	}

	public int update(PedidoVenta x, ArrayList<? extends PedidoVentaDetalle> pvdList, Usuario u) throws DAOException {
		PreparedStatement ps = null;
		PreparedStatement psPVE = null;
		PreparedStatement psPVD = null;

		int r = -1;
		Connection conn = null;
		try {
			conn = getConnectionCommiteable();
			Timestamp now = new Timestamp(System.currentTimeMillis());

			ps = conn.prepareStatement("UPDATE PEDIDO_VENTA SET SUCURSAL_ID=?,ESTADO_ID=?,FECHA_CREO=?,USUARIO_EMAIL_CREO=?,CLIENTE_ID=?,FORMA_DE_PAGO_ID=?,METODO_DE_PAGO_ID=?,FACTORIVA=?,COMENTARIOS=?,CFD_VENTA_ID=?,NUMERO_TICKET=?,CAJA=?,IMPORTE_RECIBIDO=?,APROBACION_VISA_MASTERCARD=?,PORCENTAJE_DESCUENTO_CALCULADO=?,PORCENTAJE_DESCUENTO_EXTRA=?,CONDICIONES_DE_PAGO=?,NUM_DE_CUENTA=? "
					+ " WHERE ID=?");

			int ci = 1;
			ps.setObject(ci++, x.getId());
			ps.setObject(ci++, x.getSucursalId());
			ps.setObject(ci++, x.getEstadoId());
			ps.setObject(ci++, x.getFechaCreo());
			ps.setObject(ci++, x.getUsuarioEmailCreo());
			ps.setObject(ci++, x.getClienteId());
			ps.setObject(ci++, x.getFormaDePagoId());
			ps.setObject(ci++, x.getMetodoDePagoId());
			ps.setObject(ci++, x.getFactoriva());
			ps.setObject(ci++, x.getComentarios());
			ps.setObject(ci++, x.getCfdVentaId());
			ps.setObject(ci++, x.getNumeroTicket());
			ps.setObject(ci++, x.getCaja());
			ps.setObject(ci++, x.getImporteRecibido());
			ps.setObject(ci++, x.getAprobacionVisaMastercard());
			ps.setObject(ci++, x.getPorcentajeDescuentoCalculado());
			ps.setObject(ci++, x.getPorcentajeDescuentoExtra());
			ps.setObject(ci++, x.getCondicionesDePago());
			ps.setObject(ci++, x.getNumDeCuenta());
			ps.setObject(ci++, x.getId());

			r = ps.executeUpdate();

			int rPVD = conn.createStatement().executeUpdate("DELETE FROM PEDIDO_VENTA_DETALLE WHERE PEDIDO_VENTA_ID=" + x.getId());

			psPVD = conn.prepareStatement("INSERT INTO PEDIDO_VENTA_DETALLE(PEDIDO_VENTA_ID,PRODUCTO_CODIGO_BARRAS,ALMACEN_ID,CANTIDAD,PRECIO_VENTA) "
					+ " VALUES(?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
			rPVD = 0;
			for (PedidoVentaDetalle pvd : pvdList) {
				int ciPVD = 1;

				psPVD.clearParameters();
				psPVD.clearWarnings();

				psPVD.setInt(ciPVD++, x.getId());
				if (pvd.getProductoCodigoBarras() == null) {
					psPVD.setObject(ciPVD++, null);
				} else {
					psPVD.setString(ciPVD++, pvd.getProductoCodigoBarras());
				}
				psPVD.setInt(ciPVD++, pvd.getAlmacenId());
				psPVD.setInt(ciPVD++, pvd.getCantidad());
				psPVD.setDouble(ciPVD++, pvd.getPrecioVenta());

				rPVD += psPVD.executeUpdate();
			}

			psPVE = conn.prepareStatement("UPDATE PEDIDO_VENTA_ESTADO SET FECHA=?,USUARIO_EMAIL=?,COMENTARIOS=? WHERE PEDIDO_VENTA_ID=? AND ESTADO_ID=?");
			int ciPVE = 1;

			psPVE.setTimestamp(ciPVE++, now);
			psPVE.setString(ciPVE++, u.getEmail());
			psPVE.setString(ciPVE++, "--EDITADO--");

			psPVE.setInt(ciPVE++, x.getId());
			psPVE.setInt(ciPVE++, Constants.ESTADO_VERIFICADO);

			psPVE.executeUpdate();

			conn.commit();
		} catch (SQLException ex) {
			logger.log(Level.SEVERE, "SQLException:", ex);
			try {
				conn.rollback();
			} catch (SQLException exR) {
				logger.log(Level.SEVERE, "RollBack failed:", ex);
			}
			throw new DAOException("InUpdate:" + ex.getMessage());
		} finally {
			if (ps != null) {
				try {
					ps.close();
					conn.close();
				} catch (SQLException ex) {
					logger.log(Level.SEVERE, "clossing, SQLException:" + ex.getMessage());
					throw new DAOException("Closing:" + ex.getMessage());
				}
			}
		}
		return r;
	}

	public int verificar(PedidoVenta x, Usuario u) throws DAOException {
		PreparedStatement ps = null;
		PreparedStatement psPVE = null;
		PreparedStatement psPVD = null;

		int r = -1;
		Connection conn = null;
		try {
			conn = getConnectionCommiteable();
			ps = conn.prepareStatement("UPDATE PEDIDO_VENTA SET ESTADO_ID=?,FECHA_ACTUALIZACION=? WHERE ID=?");
			Timestamp now = new Timestamp(System.currentTimeMillis());

			int ci = 1;
			ps.setInt(ci++, Constants.ESTADO_VERIFICADO);
			ps.setTimestamp(ci++, now);
			ps.setInt(ci++, x.getId());

			r = ps.executeUpdate();
			psPVE = conn.prepareStatement("INSERT INTO PEDIDO_VENTA_ESTADO(PEDIDO_VENTA_ID,ESTADO_ID,FECHA,USUARIO_EMAIL,COMENTARIOS) "
					+ " VALUES(?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);

			int ciPVE = 1;

			int rPVE = -1;

			psPVE.setInt(ciPVE++, x.getId());
			psPVE.setInt(ciPVE++, Constants.ESTADO_VERIFICADO);
			psPVE.setTimestamp(ciPVE++, x.getFechaCreo());
			psPVE.setString(ciPVE++, u.getEmail());
			psPVE.setString(ciPVE++, "--NORMAL--");

			rPVE += psPVE.executeUpdate();

			conn.commit();
		} catch (SQLException ex) {
			logger.log(Level.SEVERE, "SQLException:", ex);
			try {
				conn.rollback();
			} catch (SQLException exR) {
				logger.log(Level.SEVERE, "RollBack failed:", ex);
			}
			throw new DAOException("InUpdate:" + ex.getMessage());
		} finally {
			if (ps != null) {
				try {
					ps.close();
					conn.close();
				} catch (SQLException ex) {
					logger.log(Level.SEVERE, "clossing, SQLException:" + ex.getMessage());
					throw new DAOException("Closing:" + ex.getMessage());
				}
			}
		}
		return r;
	}

	public int surtir(PedidoVenta x, ArrayList<? extends PedidoVentaDetalle> pvdList, Usuario u) throws DAOException {
		PreparedStatement ps = null;
		PreparedStatement psPVE = null;
		PreparedStatement psPVD = null;
		PreparedStatement psMHP = null;
		int r = -1;
		Connection conn = null;
		try {
			conn = getConnectionCommiteable();

			psPVD = conn.prepareStatement("UPDATE ALMACEN_PRODUCTO SET CANTIDAD = CANTIDAD + ? "
					+ " WHERE PRODUCTO_CODIGO_BARRAS=? AND ALMACEN_ID=?");

			psMHP = conn.prepareStatement("INSERT INTO MOVIMIENTO_HISTORICO_PRODUCTO(ALMACEN_ID,FECHA,TIPO_MOVIMIENTO,CANTIDAD,COSTO,PRECIO,USUARIO_EMAIL,PRODUCTO_CODIGO_BARRAS) "
					+ " VALUES(?,?,?,?,?,?,?,?)");

			Timestamp now = new Timestamp(System.currentTimeMillis());

			for (PedidoVentaDetalle pvd : pvdList) {
				psPVD.clearParameters();

				psPVD.setInt(1, -1 * pvd.getCantidad());
				psPVD.setString(2, pvd.getProductoCodigoBarras());
				psPVD.setInt(3, pvd.getAlmacenId());

				psPVD.executeUpdate();

				int ci = 1;
				psMHP.clearParameters();

				psMHP.setInt(ci++, pvd.getAlmacenId());
				psMHP.setTimestamp(ci++, now);
				psMHP.setInt(ci++, Constants.TIPO_MOV_SALIDA_ALMACEN);
				psMHP.setInt(ci++, pvd.getCantidad());
				psMHP.setObject(ci++, null);
				psMHP.setObject(ci++, null);
				psMHP.setString(ci++, u.getEmail());
				psMHP.setString(ci++, pvd.getProductoCodigoBarras());

				r = psMHP.executeUpdate();

			}
			psPVD.close();
			psMHP.close();

			ps = conn.prepareStatement("UPDATE PEDIDO_VENTA SET ESTADO_ID=?,FECHA_ACTUALIZACION=? WHERE ID=?");

			int ci = 1;
			ps.setInt(ci++, Constants.ESTADO_SURTIDO);
			ps.setTimestamp(ci++, now);
			ps.setInt(ci++, x.getId());

			r = ps.executeUpdate();
			psPVE = conn.prepareStatement("INSERT INTO PEDIDO_VENTA_ESTADO(PEDIDO_VENTA_ID,ESTADO_ID,FECHA,USUARIO_EMAIL,COMENTARIOS) "
					+ " VALUES(?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);

			int ciPVE = 1;

			int rPVE = -1;
			psPVE.clearParameters();
			ciPVE = 1;

			psPVE.setInt(ciPVE++, x.getId());
			psPVE.setInt(ciPVE++, Constants.ESTADO_SURTIDO);
			psPVE.setTimestamp(ciPVE++, now);
			psPVE.setString(ciPVE++, u.getEmail());
			psPVE.setString(ciPVE++, "--NORMAL--");

			rPVE += psPVE.executeUpdate();

			conn.commit();

		} catch (SQLException ex) {
			logger.log(Level.SEVERE, "SQLException:", ex);
			try {
				conn.rollback();
			} catch (SQLException exR) {
				logger.log(Level.SEVERE, "RollBack failed:", ex);
			}
			throw new DAOException("InUpdate:" + ex.getMessage());
		} finally {
			if (ps != null) {
				try {
					ps.close();
					conn.close();
				} catch (SQLException ex) {
					logger.log(Level.SEVERE, "clossing, SQLException:" + ex.getMessage());
					throw new DAOException("Closing:" + ex.getMessage());
				}
			}
		}
		return r;
	}

	public int update(PedidoVenta x) throws DAOException {
		PreparedStatement ps = null;
		int r = -1;
		Connection conn = null;
		try {
			conn = getConnection();
			ps = conn.prepareStatement("UPDATE PEDIDO_VENTA SET SUCURSAL_ID=?,ESTADO_ID=?,FECHA_ACTUALIZACION=?,USUARIO_EMAIL=?,CLIENTE_ID=?,FORMA_DE_PAGO_ID=?,METODO_DE_PAGO_ID=?,FACTORIVA=?,COMENTARIOS=?,DESCUENTO_APLICADO=?,CFD_VENTA_ID=?,NUMERO_TICKET=?,CAJA=?,IMPORTE_RECIBIDO=?,APROBACION_VISA_MASTERCARD=?,PORCENTAJE_DESCUENTO_CALCULADO=?,PORCENTAJE_DESCUENTO_EXTRA=?,CONDICIONES_DE_PAGO=?,NUM_DE_CUENTA=? "
					+ " WHERE ID=?");

			int ci = 1;
			ps.setInt(ci++, x.getSucursalId());
			ps.setInt(ci++, x.getEstadoId());
			ps.setTimestamp(ci++, x.getFechaCreo());
			ps.setString(ci++, x.getUsuarioEmailCreo());
			if (x.getClienteId() == null) {
				ps.setObject(ci++, null);
			} else {
				ps.setInt(ci++, x.getClienteId());
			}
			if (x.getFormaDePagoId() == null) {
				ps.setObject(ci++, null);
			} else {
				ps.setInt(ci++, x.getFormaDePagoId());
			}
			if (x.getMetodoDePagoId() == null) {
				ps.setObject(ci++, null);
			} else {
				ps.setInt(ci++, x.getMetodoDePagoId());
			}
			ps.setDouble(ci++, x.getFactoriva());
			if (x.getComentarios() == null) {
				ps.setObject(ci++, null);
			} else {
				ps.setString(ci++, x.getComentarios());
			}
			if (x.getCfdVentaId() == null) {
				ps.setObject(ci++, null);
			} else {
				ps.setInt(ci++, x.getCfdVentaId());
			}
			if (x.getNumeroTicket() == null) {
				ps.setObject(ci++, null);
			} else {
				ps.setString(ci++, x.getNumeroTicket());
			}
			if (x.getCaja() == null) {
				ps.setObject(ci++, null);
			} else {
				ps.setInt(ci++, x.getCaja());
			}
			if (x.getImporteRecibido() == null) {
				ps.setObject(ci++, null);
			} else {
				ps.setDouble(ci++, x.getImporteRecibido());
			}
			if (x.getAprobacionVisaMastercard() == null) {
				ps.setObject(ci++, null);
			} else {
				ps.setString(ci++, x.getAprobacionVisaMastercard());
			}
			if (x.getPorcentajeDescuentoCalculado() == null) {
				ps.setObject(ci++, null);
			} else {
				ps.setInt(ci++, x.getPorcentajeDescuentoCalculado());
			}
			if (x.getPorcentajeDescuentoExtra() == null) {
				ps.setObject(ci++, null);
			} else {
				ps.setInt(ci++, x.getPorcentajeDescuentoExtra());
			}
			if (x.getCondicionesDePago() == null) {
				ps.setObject(ci++, null);
			} else {
				ps.setString(ci++, x.getCondicionesDePago());
			}
			if (x.getNumDeCuenta() == null) {
				ps.setObject(ci++, null);
			} else {
				ps.setString(ci++, x.getNumDeCuenta());
			}
			ps.setInt(ci++, x.getId());

			r = ps.executeUpdate();
		} catch (SQLException ex) {
			logger.log(Level.SEVERE, "SQLException:", ex);
			try {
				conn.rollback();
			} catch (SQLException exR) {
				logger.log(Level.SEVERE, "RollBack failed:", ex);
			}
			throw new DAOException("InUpdate:" + ex.getMessage());
		} finally {
			if (ps != null) {
				try {
					ps.close();
					conn.close();
				} catch (SQLException ex) {
					logger.log(Level.SEVERE, "clossing, SQLException:" + ex.getMessage());
					throw new DAOException("Closing:" + ex.getMessage());
				}
			}
		}
		return r;
	}

	public int delete(PedidoVenta x) throws DAOException {
		PreparedStatement ps = null;
		int r = -1;
		Connection conn = null;
		try {
			conn = getConnection();
			ps = conn.prepareStatement("DELETE FROM PEDIDO_VENTA WHERE ID=?");
			ps.setInt(1, x.getId());

			r = ps.executeUpdate();
		} catch (SQLException ex) {
			logger.log(Level.SEVERE, "delete:", ex);
			throw new DAOException("InUpdate:" + ex.getMessage());
		} finally {
			if (ps != null) {
				try {
					ps.close();
					conn.close();
				} catch (SQLException ex) {
					logger.log(Level.SEVERE, "delete:clossing:", ex);
				}
			}
		}
		return r;
	}

}
