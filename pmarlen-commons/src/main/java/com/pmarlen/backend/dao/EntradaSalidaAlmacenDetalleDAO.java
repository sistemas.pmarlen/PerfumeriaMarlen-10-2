/**
 * EntradaSalidaAlmacenDetalleDAO
 *
 * Created 2015/02/05 19:06
 *
 * @author tracktopell :: DAO Builder
 * http://www.tracktopell.com.mx
 */

package com.pmarlen.backend.dao;

import java.util.ArrayList;

import java.io.ByteArrayInputStream;

import java.sql.SQLException;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Blob;
import java.sql.Timestamp;	

import java.util.logging.Logger;

import com.pmarlen.backend.model.*;
import com.tracktopell.jdbc.DataSourceFacade;

/**
 * Class for EntradaSalidaAlmacenDetalleDAO of Table ENTRADA_SALIDA_ALMACEN_DETALLE.
 * 
 * @author Tracktopell::jpa-builder @see  https://github.com/tracktopell/UtilProjects/tree/master/jpa-builder
 * @date 2015/02/05 19:06
 */

public class EntradaSalidaAlmacenDetalleDAO {

	private final static Logger logger = Logger.getLogger(EntradaSalidaAlmacenDetalleDAO.class.getName());

	/**
	*	Datasource for table ENTRADA_SALIDA_ALMACEN_DETALLE simple CRUD operations.
	*   x common paramenter.
	*/

	private static EntradaSalidaAlmacenDetalleDAO instance;

	private EntradaSalidaAlmacenDetalleDAO(){	
		logger.fine("created EntradaSalidaAlmacenDetalleDAO.");
	}

	public static EntradaSalidaAlmacenDetalleDAO getInstance() {
		if(instance == null){
			instance = new EntradaSalidaAlmacenDetalleDAO();
		}
		return instance;
	}

	private Connection getConnection(){
		return DataSourceFacade.getStrategy().getConnection();
	}

	private Connection getConnectionCommiteable(){
		return DataSourceFacade.getStrategy().getConnectionCommiteable();
	}

    public EntradaSalidaAlmacenDetalle findBy(EntradaSalidaAlmacenDetalle x) throws DAOException, EntityNotFoundException{
		EntradaSalidaAlmacenDetalle r = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		try {
			conn = getConnection();
			ps = conn.prepareStatement("SELECT ID,ENTRADA_SALIDA_ALMACEN_ID,PRODUCTO_CODIGO_BARRAS,ALMACEN_ID,CANTIDAD,PRECIO_VENTA,COSTO FROM ENTRADA_SALIDA_ALMACEN_DETALLE "+
					"WHERE ID=?"
			);
			ps.setInt(1, x.getId());
			
			rs = ps.executeQuery();
			if(rs.next()) {
				r = new EntradaSalidaAlmacenDetalle();
				r.setId((Integer)rs.getObject("ID"));
				r.setEntradaSalidaAlmacenId((Integer)rs.getObject("ENTRADA_SALIDA_ALMACEN_ID"));
				r.setProductoCodigoBarras((String)rs.getObject("PRODUCTO_CODIGO_BARRAS"));
				r.setAlmacenId((Integer)rs.getObject("ALMACEN_ID"));
				r.setCantidad((Integer)rs.getObject("CANTIDAD"));
				r.setPrecioVenta((Double)rs.getObject("PRECIO_VENTA"));
				r.setCosto((Double)rs.getObject("COSTO"));
			} else {
				throw new EntityNotFoundException("ENTRADA_SALIDA_ALMACEN_DETALLE NOT FOUND FOR ID="+x.getId());
			}
		}catch(SQLException ex) {
			logger.severe("SQLException:"+ ex.getMessage());
			throw new DAOException("InQuery:" + ex.getMessage());
		} finally {
			if(rs != null) {
				try{
					rs.close();
					ps.close();
					conn.close();
				}catch(SQLException ex) {
					logger.severe("clossing, SQLException:" + ex.getMessage());
					throw new DAOException("Closing:"+ex.getMessage());
				}
			}
		}
		return r;		
	}

    public ArrayList<EntradaSalidaAlmacenDetalle> findAll() throws DAOException {
		ArrayList<EntradaSalidaAlmacenDetalle> r = new ArrayList<EntradaSalidaAlmacenDetalle>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		try {
			conn = getConnection();
			ps = conn.prepareStatement("SELECT ID,ENTRADA_SALIDA_ALMACEN_ID,PRODUCTO_CODIGO_BARRAS,ALMACEN_ID,CANTIDAD,PRECIO_VENTA,COSTO FROM ENTRADA_SALIDA_ALMACEN_DETALLE");
			
			rs = ps.executeQuery();
			while(rs.next()) {
				EntradaSalidaAlmacenDetalle x = new EntradaSalidaAlmacenDetalle();
				x.setId((Integer)rs.getObject("ID"));
				x.setEntradaSalidaAlmacenId((Integer)rs.getObject("ENTRADA_SALIDA_ALMACEN_ID"));
				x.setProductoCodigoBarras((String)rs.getObject("PRODUCTO_CODIGO_BARRAS"));
				x.setAlmacenId((Integer)rs.getObject("ALMACEN_ID"));
				x.setCantidad((Integer)rs.getObject("CANTIDAD"));
				x.setPrecioVenta((Double)rs.getObject("PRECIO_VENTA"));
				x.setCosto((Double)rs.getObject("COSTO"));
				r.add(x);
			}
		}catch(SQLException ex) {
			logger.severe("SQLException:"+ ex.getMessage());
			throw new DAOException("InQuery:" + ex.getMessage());
		} finally {
			if(rs != null) {
				try{
					rs.close();
					ps.close();
					conn.close();
				}catch(SQLException ex) {
					logger.severe("clossing, SQLException:" + ex.getMessage());
					throw new DAOException("Closing:"+ex.getMessage());
				}
			}
		}
		return r;		
	};
    
    public int insert(EntradaSalidaAlmacenDetalle x) throws DAOException {
		PreparedStatement psESAD = null;
		int r = -1;
		Connection conn = null;
		try {
			conn = getConnection();
			psESAD = conn.prepareStatement("INSERT INTO ENTRADA_SALIDA_ALMACEN_DETALLE(ENTRADA_SALIDA_ALMACEN_ID,PRODUCTO_CODIGO_BARRAS,ALMACEN_ID,CANTIDAD,PRECIO_VENTA,COSTO) "+
					" VALUES(?,?,?,?,?,?)"
					,Statement.RETURN_GENERATED_KEYS);			
			int ci=1;
			psESAD.setObject(ci++,x.getId());
			psESAD.setObject(ci++,x.getEntradaSalidaAlmacenId());
			psESAD.setObject(ci++,x.getProductoCodigoBarras());
			psESAD.setObject(ci++,x.getAlmacenId());
			psESAD.setObject(ci++,x.getCantidad());
			psESAD.setObject(ci++,x.getPrecioVenta());
			psESAD.setObject(ci++,x.getCosto());

			r = psESAD.executeUpdate();					
			ResultSet rsk = psESAD.getGeneratedKeys();
			if(rsk != null){
				while(rsk.next()){
					x.setId((Integer)rsk.getObject(1));
				}
			}
		}catch(SQLException ex) {
			logger.severe("insert:" + ex.getMessage());
			throw new DAOException("InUpdate:" + ex.getMessage());
		} finally {
			if(psESAD != null) {
				try{				
					psESAD.close();
					conn.close();
				}catch(SQLException ex) {
					logger.severe("clossing, SQLException:" + ex.getMessage());
					throw new DAOException("Closing:"+ex.getMessage());
				}
			}
		}
		return r;
	}

	public int update(EntradaSalidaAlmacenDetalle x) throws DAOException {		
		PreparedStatement ps = null;
		int r= -1;
		Connection conn = null;
		try {
			conn = getConnection();
			ps = conn.prepareStatement("UPDATE ENTRADA_SALIDA_ALMACEN_DETALLE SET ENTRADA_SALIDA_ALMACEN_ID=?,PRODUCTO_CODIGO_BARRAS=?,ALMACEN_ID=?,CANTIDAD=?,PRECIO_VENTA=?,COSTO=? "+
					" WHERE ID=?");
			
			int ci=1;
			ps.setObject(ci++,x.getId());
			ps.setObject(ci++,x.getEntradaSalidaAlmacenId());
			ps.setObject(ci++,x.getProductoCodigoBarras());
			ps.setObject(ci++,x.getAlmacenId());
			ps.setObject(ci++,x.getCantidad());
			ps.setObject(ci++,x.getPrecioVenta());
			ps.setObject(ci++,x.getCosto());
			ps.setObject(ci++,x.getId());
			
			r = ps.executeUpdate();						
		}catch(SQLException ex) {
			logger.severe("update:" + ex.getMessage());
			throw new DAOException("InUpdate:" + ex.getMessage());
		} finally {
			if(ps != null) {
				try{
					ps.close();
					conn.close();
				}catch(SQLException ex) {
					logger.severe("update:clossing:" + ex.getMessage() );
					throw new DAOException("Closing:"+ex.getMessage());
				}
			}
		}
		return r;
	}

    public int delete(EntradaSalidaAlmacenDetalle x)throws DAOException {
		PreparedStatement ps = null;
		int r= -1;
		Connection conn = null;
		try {
			conn = getConnection();
			ps = conn.prepareStatement("DELETE FROM ENTRADA_SALIDA_ALMACEN_DETALLE WHERE ID=?");
			ps.setObject(1, x.getId());
			
			r = ps.executeUpdate();						
		}catch(SQLException ex) {
			logger.severe("delete:" + ex.getMessage());
			throw new DAOException("InUpdate:" + ex.getMessage());
		} finally {
			if(ps != null) {
				try{
					ps.close();
					conn.close();
				}catch(SQLException ex) {
					logger.severe("delete:clossing:" + ex.getMessage() );
					throw new DAOException("Closing:"+ex.getMessage());
				}
			}
		}
		return r;
	}

}
