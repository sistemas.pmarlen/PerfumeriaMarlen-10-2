/**
 * PedioVentaEstadoDAO
 *
 * Created 2014/12/12 18:56
 *
 * @author tracktopell :: DAO Builder http://www.tracktopell.com.mx
 */
package com.pmarlen.backend.dao;

import java.util.ArrayList;

import java.io.ByteArrayInputStream;

import java.sql.SQLException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Blob;

import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.logging.Level;

import com.pmarlen.backend.model.*;
import com.tracktopell.jdbc.DataSourceFacade;

/**
 * Class for PedioVentaEstadoDAO of Table PEDIO_VENTA_ESTADO.
 *
 * @author Tracktopell::jpa-builder @see
 * https://github.com/tracktopell/UtilProjects/tree/master/jpa-builder
 * @date 2014/12/12 18:56
 */
public class PedioVentaEstadoDAO {

	private final static Logger logger = Logger.getLogger(PedioVentaEstadoDAO.class.getName());

	/**
	 * Datasource for table PEDIO_VENTA_ESTADO simple CRUD operations. x common
	 * paramenter.
	 */
	private static PedioVentaEstadoDAO instance;

	private PedioVentaEstadoDAO() {
		logger.log(Level.SEVERE, "--> created PedioVentaEstadoDAO.");
	}

	public static PedioVentaEstadoDAO getInstance() {
		if (instance == null) {
			instance = new PedioVentaEstadoDAO();
		}
		return instance;
	}

	private Connection getConnection() {
		return DataSourceFacade.getStrategy().getConnection();
	}

	private Connection getConnectionCommiteable() {
		return DataSourceFacade.getStrategy().getConnectionCommiteable();
	}

	public PedioVentaEstado findBy(PedioVentaEstado x) {
		PedioVentaEstado r = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		try {
			conn = getConnection();
			ps = conn.prepareStatement("SELECT ID,PEDIDO_VENTA_ID,ESTADO_ID,FECHA,USUARIO_EMAIL,COMENTARIOS FROM PEDIO_VENTA_ESTADO "
					+ "WHERE ID=?"
			);
			ps.setInt(1, x.getId());

			rs = ps.executeQuery();
			if (rs.next()) {
				r = new PedioVentaEstado();
				r.setId(rs.getInt("ID"));
				r.setPedidoVentaId(rs.getInt("PEDIDO_VENTA_ID"));
				r.setEstadoId(rs.getInt("ESTADO_ID"));
				r.setFecha(rs.getDate("FECHA"));
				r.setUsuarioEmail(rs.getString("USUARIO_EMAIL"));
				r.setComentarios(rs.getString("COMENTARIOS"));
			}
		} catch (SQLException ex) {
			logger.log(Level.SEVERE, "findBy:", ex);
		} finally {
			if (rs != null) {
				try {
					rs.close();
					ps.close();
					conn.close();
				} catch (SQLException ex) {
					logger.log(Level.SEVERE, "findBy:clossing:", ex);
				}
			}
		}
		return r;
	}

	public ArrayList<PedioVentaEstado> findAll() {
		ArrayList<PedioVentaEstado> r = new ArrayList<PedioVentaEstado>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		try {
			conn = getConnection();
			ps = conn.prepareStatement("SELECT ID,PEDIDO_VENTA_ID,ESTADO_ID,FECHA,USUARIO_EMAIL,COMENTARIOS FROM PEDIO_VENTA_ESTADO");

			rs = ps.executeQuery();
			while (rs.next()) {
				PedioVentaEstado x = new PedioVentaEstado();
				x.setId(rs.getInt("ID"));
				x.setPedidoVentaId(rs.getInt("PEDIDO_VENTA_ID"));
				x.setEstadoId(rs.getInt("ESTADO_ID"));
				x.setFecha(rs.getDate("FECHA"));
				x.setUsuarioEmail(rs.getString("USUARIO_EMAIL"));
				x.setComentarios(rs.getString("COMENTARIOS"));
				r.add(x);
			}
		} catch (SQLException ex) {
			logger.log(Level.SEVERE, "findAll:", ex);
		} finally {
			if (rs != null) {
				try {
					rs.close();
					ps.close();
					conn.close();
				} catch (SQLException ex) {
					logger.log(Level.SEVERE, "findAll:clossing:", ex);
				}
			}
		}
		return r;
	}

	;
    
    public int insert(PedioVentaEstado x) {
		PreparedStatement ps = null;
		int r = -1;
		Connection conn = null;
		try {
			conn = getConnection();
			ps = conn.prepareStatement("INSERT INTO PEDIO_VENTA_ESTADO(ID,PEDIDO_VENTA_ID,ESTADO_ID,FECHA,USUARIO_EMAIL,COMENTARIOS) "
					+ " VALUES(?,?,?,?,?,?)");
			int ci = 1;
			ps.setInt(ci++, x.getId());
			ps.setInt(ci++, x.getPedidoVentaId());
			ps.setInt(ci++, x.getEstadoId());
			ps.setDate(ci++, new java.sql.Date(x.getFecha().getTime()));
			ps.setString(ci++, x.getUsuarioEmail());
			ps.setString(ci++, x.getComentarios());
			// ps.setBlob(ci++,new ByteArrayInputStream(x.getContenidoOriginalXml()));

			r = ps.executeUpdate();
		} catch (SQLException ex) {
			logger.log(Level.SEVERE, "insert:", ex);
		} finally {
			if (ps != null) {
				try {
					ps.close();
					conn.close();
				} catch (SQLException ex) {
					logger.log(Level.SEVERE, "insert:clossing:", ex);
				}
			}
		}
		return r;
	}

	public int update(PedioVentaEstado x) {
		PreparedStatement ps = null;
		int r = -1;
		Connection conn = null;
		try {
			conn = getConnection();
			ps = conn.prepareStatement("UPDATE PEDIO_VENTA_ESTADO SET PEDIDO_VENTA_ID=?,ESTADO_ID=?,FECHA=?,USUARIO_EMAIL=?,COMENTARIOS=? "
					+ " WHERE ID=?");

			int ci = 1;
			ps.setInt(ci++, x.getPedidoVentaId());
			ps.setInt(ci++, x.getEstadoId());
			ps.setDate(ci++, new java.sql.Date(x.getFecha().getTime()));
			ps.setString(ci++, x.getUsuarioEmail());
			ps.setString(ci++, x.getComentarios());
			ps.setInt(ci++, x.getId());

			r = ps.executeUpdate();
		} catch (SQLException ex) {
			logger.log(Level.SEVERE, "update:", ex);
		} finally {
			if (ps != null) {
				try {
					ps.close();
					conn.close();
				} catch (SQLException ex) {
					logger.log(Level.SEVERE, "update:clossing:", ex);
				}
			}
		}
		return r;
	}

	public int delete(PedioVentaEstado x) {
		PreparedStatement ps = null;
		int r = -1;
		Connection conn = null;
		try {
			conn = getConnection();
			ps = conn.prepareStatement("DELETE FROM PEDIO_VENTA_ESTADO WHERE ID=?");
			ps.setInt(1, x.getId());

			r = ps.executeUpdate();
		} catch (SQLException ex) {
			logger.log(Level.SEVERE, "delete:", ex);
		} finally {
			if (ps != null) {
				try {
					ps.close();
					conn.close();
				} catch (SQLException ex) {
					logger.log(Level.SEVERE, "delete:clossing:", ex);
				}
			}
		}
		return r;
	}

}
