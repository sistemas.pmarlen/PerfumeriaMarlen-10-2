/**
 * EntradaSalidaAlmacenDAO
 *
 * Created 2015/02/05 19:06
 *
 * @author tracktopell :: DAO Builder
 * http://www.tracktopell.com.mx
 */

package com.pmarlen.backend.dao;

import com.pmarlen.backend.model.*;
import com.tracktopell.jdbc.DataSourceFacade;

import java.io.ByteArrayInputStream;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Class for EntradaSalidaAlmacenDAO of Table ENTRADA_SALIDA_ALMACEN.
 * 
 * @author Tracktopell::jpa-builder @see  https://github.com/tracktopell/UtilProjects/tree/master/jpa-builder
 * @date 2015/02/05 19:06
 */

public class EntradaSalidaAlmacenDAO {

	private final static Logger logger = Logger.getLogger(EntradaSalidaAlmacenDAO.class.getName());

	/**
	*	Datasource for table ENTRADA_SALIDA_ALMACEN simple CRUD operations.
	*   x common paramenter.
	*/

	private static EntradaSalidaAlmacenDAO instance;

	private EntradaSalidaAlmacenDAO(){	
		logger.fine("created EntradaSalidaAlmacenDAO.");
	}

	public static EntradaSalidaAlmacenDAO getInstance() {
		if(instance == null){
			instance = new EntradaSalidaAlmacenDAO();
		}
		return instance;
	}

	private Connection getConnection(){
		return DataSourceFacade.getStrategy().getConnection();
	}

	private Connection getConnectionCommiteable(){
		return DataSourceFacade.getStrategy().getConnectionCommiteable();
	}

    public EntradaSalidaAlmacen findBy(EntradaSalidaAlmacen x) throws DAOException, EntityNotFoundException{
		EntradaSalidaAlmacen r = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		try {
			conn = getConnection();
			ps = conn.prepareStatement("SELECT ID,SUCURSAL_ID,USUARIO_EMAIL,TIPO_MOVIMIENTO,FECHA,COMENTARIOS,DESCUENTO_APLICADO,ESTADO,CFD_ID,CLIENTE_ID,PEDIDO_VENTA_ID FROM ENTRADA_SALIDA_ALMACEN "+
					"WHERE ID=?"
			);
			ps.setInt(1, x.getId());
			
			rs = ps.executeQuery();
			if(rs.next()) {
				r = new EntradaSalidaAlmacen();
				r.setId((Integer)rs.getObject("ID"));
				r.setSucursalId((Integer)rs.getObject("SUCURSAL_ID"));
				r.setUsuarioEmail((String)rs.getObject("USUARIO_EMAIL"));
				r.setTipoMovimiento((Integer)rs.getObject("TIPO_MOVIMIENTO"));
				r.setFecha((Timestamp)rs.getObject("FECHA"));
				r.setComentarios((String)rs.getObject("COMENTARIOS"));
				r.setDescuentoAplicado((Double)rs.getObject("DESCUENTO_APLICADO"));
				r.setEstado((Integer)rs.getObject("ESTADO"));
				r.setCfdId((Integer)rs.getObject("CFD_ID"));
				r.setClienteId((Integer)rs.getObject("CLIENTE_ID"));
				r.setPedidoVentaId((Integer)rs.getObject("PEDIDO_VENTA_ID"));
			} else {
				throw new EntityNotFoundException("ENTRADA_SALIDA_ALMACEN NOT FOUND FOR ID="+x.getId());
			}
		}catch(SQLException ex) {
			logger.severe("SQLException:"+ ex.getMessage());
			throw new DAOException("InQuery:" + ex.getMessage());
		} finally {
			if(rs != null) {
				try{
					rs.close();
					ps.close();
					conn.close();
				}catch(SQLException ex) {
					logger.severe("clossing, SQLException:" + ex.getMessage());
					throw new DAOException("Closing:"+ex.getMessage());
				}
			}
		}
		return r;		
	}

    public ArrayList<EntradaSalidaAlmacen> findAll() throws DAOException {
		ArrayList<EntradaSalidaAlmacen> r = new ArrayList<EntradaSalidaAlmacen>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		try {
			conn = getConnection();
			ps = conn.prepareStatement("SELECT ID,SUCURSAL_ID,USUARIO_EMAIL,TIPO_MOVIMIENTO,FECHA,COMENTARIOS,DESCUENTO_APLICADO,ESTADO,CFD_ID,CLIENTE_ID,PEDIDO_VENTA_ID FROM ENTRADA_SALIDA_ALMACEN");
			
			rs = ps.executeQuery();
			while(rs.next()) {
				EntradaSalidaAlmacen x = new EntradaSalidaAlmacen();
				x.setId((Integer)rs.getObject("ID"));
				x.setSucursalId((Integer)rs.getObject("SUCURSAL_ID"));
				x.setUsuarioEmail((String)rs.getObject("USUARIO_EMAIL"));
				x.setTipoMovimiento((Integer)rs.getObject("TIPO_MOVIMIENTO"));
				x.setFecha((Timestamp)rs.getObject("FECHA"));
				x.setComentarios((String)rs.getObject("COMENTARIOS"));
				x.setDescuentoAplicado((Double)rs.getObject("DESCUENTO_APLICADO"));
				x.setEstado((Integer)rs.getObject("ESTADO"));
				x.setCfdId((Integer)rs.getObject("CFD_ID"));
				x.setClienteId((Integer)rs.getObject("CLIENTE_ID"));
				x.setPedidoVentaId((Integer)rs.getObject("PEDIDO_VENTA_ID"));
				r.add(x);
			}
		}catch(SQLException ex) {
			logger.severe("SQLException:"+ ex.getMessage());
			throw new DAOException("InQuery:" + ex.getMessage());
		} finally {
			if(rs != null) {
				try{
					rs.close();
					ps.close();
					conn.close();
				}catch(SQLException ex) {
					logger.severe("clossing, SQLException:" + ex.getMessage());
					throw new DAOException("Closing:"+ex.getMessage());
				}
			}
		}
		return r;		
	};
    public int insert(EntradaSalidaAlmacen x,List<EntradaSalidaAlmacenDetalle> esadList) throws DAOException {
		PreparedStatement ps = null;
		PreparedStatement psESAD = null;
		int r = -1;
		Connection conn = null;
		try {
			conn = getConnection();
			ps = conn.prepareStatement("INSERT INTO ENTRADA_SALIDA_ALMACEN(ID,SUCURSAL_ID,USUARIO_EMAIL,TIPO_MOVIMIENTO,FECHA,COMENTARIOS,DESCUENTO_APLICADO,ESTADO,CFD_ID,CLIENTE_ID,PEDIDO_VENTA_ID) "+
					" VALUES(?,?,?,?,?,?,?,?,?,?,?)"
					,Statement.RETURN_GENERATED_KEYS);			
			int ci=1;
			ps.setObject(ci++,x.getId());
			ps.setObject(ci++,x.getSucursalId());
			ps.setObject(ci++,x.getUsuarioEmail());
			ps.setObject(ci++,x.getTipoMovimiento());
			ps.setObject(ci++,x.getFecha());
			ps.setObject(ci++,x.getComentarios());
			ps.setObject(ci++,x.getDescuentoAplicado());
			ps.setObject(ci++,x.getEstado());
			ps.setObject(ci++,x.getCfdId());
			ps.setObject(ci++,x.getClienteId());
			ps.setObject(ci++,x.getPedidoVentaId());

			r = ps.executeUpdate();					
			ResultSet rsk = ps.getGeneratedKeys();
			if(rsk != null){
				while(rsk.next()){
					x.setId((Integer)rsk.getObject(1));
				}
			}
		}catch(SQLException ex) {
			logger.severe("insert:" + ex.getMessage());
			throw new DAOException("InUpdate:" + ex.getMessage());
		} finally {
			if(ps != null) {
				try{				
					ps.close();
					conn.close();
				}catch(SQLException ex) {
					logger.severe("clossing, SQLException:" + ex.getMessage());
					throw new DAOException("Closing:"+ex.getMessage());
				}
			}
		}
		return r;
	}
    
    public int insert(EntradaSalidaAlmacen x) throws DAOException {
		PreparedStatement ps = null;
		int r = -1;
		Connection conn = null;
		try {
			conn = getConnection();
			ps = conn.prepareStatement("INSERT INTO ENTRADA_SALIDA_ALMACEN(ID,SUCURSAL_ID,USUARIO_EMAIL,TIPO_MOVIMIENTO,FECHA,COMENTARIOS,DESCUENTO_APLICADO,ESTADO,CFD_ID,CLIENTE_ID,PEDIDO_VENTA_ID) "+
					" VALUES(?,?,?,?,?,?,?,?,?,?,?)"
					,Statement.RETURN_GENERATED_KEYS);			
			int ci=1;
			ps.setObject(ci++,x.getId());
			ps.setObject(ci++,x.getSucursalId());
			ps.setObject(ci++,x.getUsuarioEmail());
			ps.setObject(ci++,x.getTipoMovimiento());
			ps.setObject(ci++,x.getFecha());
			ps.setObject(ci++,x.getComentarios());
			ps.setObject(ci++,x.getDescuentoAplicado());
			ps.setObject(ci++,x.getEstado());
			ps.setObject(ci++,x.getCfdId());
			ps.setObject(ci++,x.getClienteId());
			ps.setObject(ci++,x.getPedidoVentaId());

			r = ps.executeUpdate();					
			ResultSet rsk = ps.getGeneratedKeys();
			if(rsk != null){
				while(rsk.next()){
					x.setId((Integer)rsk.getObject(1));
				}
			}
		}catch(SQLException ex) {
			logger.severe("insert:" + ex.getMessage());
			throw new DAOException("InUpdate:" + ex.getMessage());
		} finally {
			if(ps != null) {
				try{				
					ps.close();
					conn.close();
				}catch(SQLException ex) {
					logger.severe("clossing, SQLException:" + ex.getMessage());
					throw new DAOException("Closing:"+ex.getMessage());
				}
			}
		}
		return r;
	}

	public int update(EntradaSalidaAlmacen x) throws DAOException {		
		PreparedStatement ps = null;
		int r= -1;
		Connection conn = null;
		try {
			conn = getConnection();
			ps = conn.prepareStatement("UPDATE ENTRADA_SALIDA_ALMACEN SET SUCURSAL_ID=?,USUARIO_EMAIL=?,TIPO_MOVIMIENTO=?,FECHA=?,COMENTARIOS=?,DESCUENTO_APLICADO=?,ESTADO=?,CFD_ID=?,CLIENTE_ID=?,PEDIDO_VENTA_ID=? "+
					" WHERE ID=?");
			
			int ci=1;
			ps.setObject(ci++,x.getId());
			ps.setObject(ci++,x.getSucursalId());
			ps.setObject(ci++,x.getUsuarioEmail());
			ps.setObject(ci++,x.getTipoMovimiento());
			ps.setObject(ci++,x.getFecha());
			ps.setObject(ci++,x.getComentarios());
			ps.setObject(ci++,x.getDescuentoAplicado());
			ps.setObject(ci++,x.getEstado());
			ps.setObject(ci++,x.getCfdId());
			ps.setObject(ci++,x.getClienteId());
			ps.setObject(ci++,x.getPedidoVentaId());
			ps.setObject(ci++,x.getId());
			
			r = ps.executeUpdate();						
		}catch(SQLException ex) {
			logger.severe("update:" + ex.getMessage());
			throw new DAOException("InUpdate:" + ex.getMessage());
		} finally {
			if(ps != null) {
				try{
					ps.close();
					conn.close();
				}catch(SQLException ex) {
					logger.severe("update:clossing:" + ex.getMessage() );
					throw new DAOException("Closing:"+ex.getMessage());
				}
			}
		}
		return r;
	}

    public int delete(EntradaSalidaAlmacen x)throws DAOException {
		PreparedStatement ps = null;
		int r= -1;
		Connection conn = null;
		try {
			conn = getConnection();
			ps = conn.prepareStatement("DELETE FROM ENTRADA_SALIDA_ALMACEN WHERE ID=?");
			ps.setObject(1, x.getId());
			
			r = ps.executeUpdate();						
		}catch(SQLException ex) {
			logger.severe("delete:" + ex.getMessage());
			throw new DAOException("InUpdate:" + ex.getMessage());
		} finally {
			if(ps != null) {
				try{
					ps.close();
					conn.close();
				}catch(SQLException ex) {
					logger.severe("delete:clossing:" + ex.getMessage() );
					throw new DAOException("Closing:"+ex.getMessage());
				}
			}
		}
		return r;
	}

}
