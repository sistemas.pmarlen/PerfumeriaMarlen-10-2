/**
 * AlmacenDAO
 *
 * Created 2015/01/30 18:24
 *
 * @author tracktopell :: DAO Builder
 * http://www.tracktopell.com.mx
 */

package com.pmarlen.backend.dao;

import com.pmarlen.backend.model.*;
import com.tracktopell.jdbc.DataSourceFacade;

import java.io.ByteArrayInputStream;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;	

import java.util.ArrayList; 
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class for AlmacenDAO of Table ALMACEN.
 * 
 * @author Tracktopell::jpa-builder @see  https://github.com/tracktopell/UtilProjects/tree/master/jpa-builder
 * @date 2015/01/30 18:24
 */

public class AlmacenDAO {

	private final static Logger logger = Logger.getLogger(AlmacenDAO.class.getName());

	/**
	*	Datasource for table ALMACEN simple CRUD operations.
	*   x common paramenter.
	*/

	private static AlmacenDAO instance;

	private AlmacenDAO(){	
		logger.fine("created AlmacenDAO.");
	}

	public static AlmacenDAO getInstance() {
		if(instance == null){
			instance = new AlmacenDAO();
		}
		return instance;
	}

	private Connection getConnection(){
		return DataSourceFacade.getStrategy().getConnection();
	}

	private Connection getConnectionCommiteable(){
		return DataSourceFacade.getStrategy().getConnectionCommiteable();
	}

    public Almacen findBy(Almacen x) throws DAOException, EntityNotFoundException{
		Almacen r = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		try {
			conn = getConnection();
			ps = conn.prepareStatement("SELECT ID,TIPO_ALMACEN,SUCURSAL_ID FROM ALMACEN "+
					"WHERE ID=?"
			);
			ps.setInt(1, x.getId());
			
			rs = ps.executeQuery();
			if(rs.next()) {
				r = new Almacen();
				r.setId((Integer)rs.getObject("ID"));
				r.setTipoAlmacen((Integer)rs.getObject("TIPO_ALMACEN"));
				r.setSucursalId((Integer)rs.getObject("SUCURSAL_ID"));
			} else {
				throw new EntityNotFoundException("ALMACEN NOT FOUND FOR ID="+x.getId());
			}
		}catch(SQLException ex) {
			logger.log(Level.SEVERE,"SQLException:", ex);
			throw new DAOException("InQuery:" + ex.getMessage());
		} finally {
			if(rs != null) {
				try{
					rs.close();
					ps.close();
					conn.close();
				}catch(SQLException ex) {
					logger.log(Level.SEVERE,"clossing, SQLException:" + ex.getMessage());
					throw new DAOException("Closing:"+ex.getMessage());
				}
			}
		}
		return r;		
	}

    public ArrayList<Almacen> findAll() throws DAOException {
		ArrayList<Almacen> r = new ArrayList<Almacen>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		try {
			conn = getConnection();
			ps = conn.prepareStatement("SELECT ID,TIPO_ALMACEN,SUCURSAL_ID FROM ALMACEN");
			
			rs = ps.executeQuery();
			while(rs.next()) {
				Almacen x = new Almacen();
				x.setId((Integer)rs.getObject("ID"));
				x.setTipoAlmacen((Integer)rs.getObject("TIPO_ALMACEN"));
				x.setSucursalId((Integer)rs.getObject("SUCURSAL_ID"));
				r.add(x);
			}
		}catch(SQLException ex) {
			logger.log(Level.SEVERE,"SQLException:", ex);
			throw new DAOException("InQuery:" + ex.getMessage());
		} finally {
			if(rs != null) {
				try{
					rs.close();
					ps.close();
					conn.close();
				}catch(SQLException ex) {
					logger.log(Level.SEVERE,"clossing, SQLException:" + ex.getMessage());
					throw new DAOException("Closing:"+ex.getMessage());
				}
			}
		}
		return r;		
	};
    
    public int insert(Almacen x) throws DAOException {
		PreparedStatement ps = null;
		int r = -1;
		Connection conn = null;
		try {
			conn = getConnection();
			ps = conn.prepareStatement("INSERT INTO ALMACEN(TIPO_ALMACEN,SUCURSAL_ID) "+
					" VALUES(?,?)"
					,Statement.RETURN_GENERATED_KEYS);			
			int ci=1;
			ps.setObject(ci++,x.getId());
			ps.setObject(ci++,x.getTipoAlmacen());
			ps.setObject(ci++,x.getSucursalId());

			r = ps.executeUpdate();					
			ResultSet rsk = ps.getGeneratedKeys();
			if(rsk != null){
				while(rsk.next()){
					x.setId((Integer)rsk.getObject(1));
				}
			}
		}catch(SQLException ex) {
			logger.severe("insert:" + ex.getMessage());
			throw new DAOException("InUpdate:" + ex.getMessage());
		} finally {
			if(ps != null) {
				try{				
					ps.close();
					conn.close();
				}catch(SQLException ex) {
					logger.log(Level.SEVERE,"clossing, SQLException:" + ex.getMessage());
					throw new DAOException("Closing:"+ex.getMessage());
				}
			}
		}
		return r;
	}

	public int update(Almacen x) throws DAOException {		
		PreparedStatement ps = null;
		int r= -1;
		Connection conn = null;
		try {
			conn = getConnection();
			ps = conn.prepareStatement("UPDATE ALMACEN SET TIPO_ALMACEN=?,SUCURSAL_ID=? "+
					" WHERE ID=?");
			
			int ci=1;
			ps.setObject(ci++,x.getId());
			ps.setObject(ci++,x.getTipoAlmacen());
			ps.setObject(ci++,x.getSucursalId());
			ps.setObject(ci++,x.getId());
			
			r = ps.executeUpdate();						
		}catch(SQLException ex) {
			logger.severe("update:" + ex.getMessage());
			throw new DAOException("InUpdate:" + ex.getMessage());
		} finally {
			if(ps != null) {
				try{
					ps.close();
					conn.close();
				}catch(SQLException ex) {
					logger.severe("update:clossing:" + ex.getMessage() );
					throw new DAOException("Closing:"+ex.getMessage());
				}
			}
		}
		return r;
	}

    public int delete(Almacen x)throws DAOException {
		PreparedStatement ps = null;
		int r= -1;
		Connection conn = null;
		try {
			conn = getConnection();
			ps = conn.prepareStatement("DELETE FROM ALMACEN WHERE ID=?");
			ps.setObject(1, x.getId());
			
			r = ps.executeUpdate();						
		}catch(SQLException ex) {
			logger.severe("delete:" + ex.getMessage());
			throw new DAOException("InUpdate:" + ex.getMessage());
		} finally {
			if(ps != null) {
				try{
					ps.close();
					conn.close();
				}catch(SQLException ex) {
					logger.severe("delete:clossing:" + ex.getMessage() );
					throw new DAOException("Closing:"+ex.getMessage());
				}
			}
		}
		return r;
	}

}
