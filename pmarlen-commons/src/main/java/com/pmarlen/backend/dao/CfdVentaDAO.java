/**
 * CfdVentaDAO
 *
 * Created 2014/12/29 17:59
 *
 * @author tracktopell :: DAO Builder http://www.tracktopell.com.mx
 */
package com.pmarlen.backend.dao;

import java.util.ArrayList;

import java.io.ByteArrayInputStream;

import java.sql.SQLException;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Blob;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.pmarlen.backend.model.*;
import com.tracktopell.jdbc.DataSourceFacade;

/**
 * Class for CfdVentaDAO of Table CFD_VENTA.
 *
 * @author Tracktopell::jpa-builder @see
 * https://github.com/tracktopell/UtilProjects/tree/master/jpa-builder
 * @date 2014/12/29 17:59
 */
public class CfdVentaDAO {

	private final static Logger logger = Logger.getLogger(CfdVentaDAO.class.getName());

	/**
	 * Datasource for table CFD_VENTA simple CRUD operations. x common
	 * paramenter.
	 */
	private static CfdVentaDAO instance;

	private CfdVentaDAO() {
		logger.log(Level.SEVERE, "--> created CfdVentaDAO.");
	}

	public static CfdVentaDAO getInstance() {
		if (instance == null) {
			instance = new CfdVentaDAO();
		}
		return instance;
	}

	private Connection getConnection() {
		return DataSourceFacade.getStrategy().getConnection();
	}

	private Connection getConnectionCommiteable() {
		return DataSourceFacade.getStrategy().getConnectionCommiteable();
	}

	public CfdVenta findBy(CfdVenta x) {
		CfdVenta r = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		try {
			conn = getConnection();
			ps = conn.prepareStatement("SELECT ID,ULTIMA_ACTUALIZACION,CONTENIDO_ORIGINAL_XML,CALLING_ERROR_RESULT FROM CFD_VENTA "
					+ "WHERE ID=?"
			);
			ps.setInt(1, x.getId());

			rs = ps.executeQuery();
			if (rs.next()) {
				r = new CfdVenta();
				r.setId(rs.getInt("ID"));
				r.setUltimaActualizacion(rs.getTimestamp("ULTIMA_ACTUALIZACION"));
				Blob bc = rs.getBlob("CONTENIDO_ORIGINAL_XML");
				r.setContenidoOriginalXml(bc.getBytes(0, (int) bc.length()));
				r.setCallingErrorResult(rs.getString("CALLING_ERROR_RESULT"));
			}
		} catch (SQLException ex) {
			logger.log(Level.SEVERE, "findBy:", ex);
		} finally {
			if (rs != null) {
				try {
					rs.close();
					ps.close();
					conn.close();
				} catch (SQLException ex) {
					logger.log(Level.SEVERE, "findBy:clossing:", ex);
				}
			}
		}
		return r;
	}

	public ArrayList<CfdVenta> findAll() {
		ArrayList<CfdVenta> r = new ArrayList<CfdVenta>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		try {
			conn = getConnection();
			ps = conn.prepareStatement("SELECT ID,ULTIMA_ACTUALIZACION,CONTENIDO_ORIGINAL_XML,CALLING_ERROR_RESULT FROM CFD_VENTA");

			rs = ps.executeQuery();
			while (rs.next()) {
				CfdVenta x = new CfdVenta();
				x.setId(rs.getInt("ID"));
				x.setUltimaActualizacion(rs.getTimestamp("ULTIMA_ACTUALIZACION"));
				Blob bc = rs.getBlob("CONTENIDO_ORIGINAL_XML");
				x.setContenidoOriginalXml(bc.getBytes(0, (int) bc.length()));
				x.setCallingErrorResult(rs.getString("CALLING_ERROR_RESULT"));
				r.add(x);
			}
		} catch (SQLException ex) {
			logger.log(Level.SEVERE, "findAll:", ex);
		} finally {
			if (rs != null) {
				try {
					rs.close();
					ps.close();
					conn.close();
				} catch (SQLException ex) {
					logger.log(Level.SEVERE, "findAll:clossing:", ex);
				}
			}
		}
		return r;
	}

	;
    
    public int insert(CfdVenta x) {
		PreparedStatement ps = null;
		int r = -1;
		Connection conn = null;
		try {
			conn = getConnection();
			ps = conn.prepareStatement("INSERT INTO CFD_VENTA(ULTIMA_ACTUALIZACION,CONTENIDO_ORIGINAL_XML,CALLING_ERROR_RESULT) "
					+ " VALUES(?,?,?)", Statement.RETURN_GENERATED_KEYS);
			int ci = 1;
			//ps.setInt(ci++,x.getId());
			ps.setTimestamp(ci++, x.getUltimaActualizacion());
			if (x.getContenidoOriginalXml() == null) {
				ps.setObject(ci++, null);
			} else {
				ps.setBlob(ci++, new ByteArrayInputStream(x.getContenidoOriginalXml()));
			}
			if (x.getCallingErrorResult() == null) {
				ps.setObject(ci++, null);
			} else {
				ps.setString(ci++, x.getCallingErrorResult());
			}

			r = ps.executeUpdate();
			ResultSet rsk = ps.getGeneratedKeys();
			if (rsk != null) {
				while (rsk.next()) {
					x.setId(rsk.getInt(1));
				}
			}
		} catch (SQLException ex) {
			logger.log(Level.SEVERE, "insert:", ex);
		} finally {
			if (ps != null) {
				try {
					ps.close();
					conn.close();
				} catch (SQLException ex) {
					logger.log(Level.SEVERE, "insert:clossing:", ex);
				}
			}
		}
		return r;
	}

	public int update(CfdVenta x) {
		PreparedStatement ps = null;
		int r = -1;
		Connection conn = null;
		try {
			conn = getConnection();
			ps = conn.prepareStatement("UPDATE CFD_VENTA SET ULTIMA_ACTUALIZACION=?,CONTENIDO_ORIGINAL_XML=?,CALLING_ERROR_RESULT=? "
					+ " WHERE ID=?");

			int ci = 1;
			ps.setTimestamp(ci++, x.getUltimaActualizacion());
			if (x.getContenidoOriginalXml() == null) {
				ps.setObject(ci++, null);
			} else {
				ps.setBlob(ci++, new ByteArrayInputStream(x.getContenidoOriginalXml()));
			}
			if (x.getCallingErrorResult() == null) {
				ps.setObject(ci++, null);
			} else {
				ps.setString(ci++, x.getCallingErrorResult());
			}
			ps.setInt(ci++, x.getId());

			r = ps.executeUpdate();
		} catch (SQLException ex) {
			logger.log(Level.SEVERE, "update:", ex);
		} finally {
			if (ps != null) {
				try {
					ps.close();
					conn.close();
				} catch (SQLException ex) {
					logger.log(Level.SEVERE, "update:clossing:", ex);
				}
			}
		}
		return r;
	}

	public int delete(CfdVenta x) {
		PreparedStatement ps = null;
		int r = -1;
		Connection conn = null;
		try {
			conn = getConnection();
			ps = conn.prepareStatement("DELETE FROM CFD_VENTA WHERE ID=?");
			ps.setInt(1, x.getId());

			r = ps.executeUpdate();
		} catch (SQLException ex) {
			logger.log(Level.SEVERE, "delete:", ex);
		} finally {
			if (ps != null) {
				try {
					ps.close();
					conn.close();
				} catch (SQLException ex) {
					logger.log(Level.SEVERE, "delete:clossing:", ex);
				}
			}
		}
		return r;
	}

}
