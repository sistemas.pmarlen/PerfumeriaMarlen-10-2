/**
 * PedidoVentaDetalleDAO
 *
 * Created 2015/01/30 18:24
 *
 * @author tracktopell :: DAO Builder http://www.tracktopell.com.mx
 */
package com.pmarlen.backend.dao;

import java.util.ArrayList;

import java.io.ByteArrayInputStream;

import java.sql.SQLException;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Blob;
import java.sql.Timestamp;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.pmarlen.backend.model.*;
import com.tracktopell.jdbc.DataSourceFacade;

/**
 * Class for PedidoVentaDetalleDAO of Table PEDIDO_VENTA_DETALLE.
 *
 * @author Tracktopell::jpa-builder @see
 * https://github.com/tracktopell/UtilProjects/tree/master/jpa-builder
 * @date 2015/01/30 18:24
 */
public class PedidoVentaDetalleDAO {

	private final static Logger logger = Logger.getLogger(PedidoVentaDetalleDAO.class.getName());

	/**
	 * Datasource for table PEDIDO_VENTA_DETALLE simple CRUD operations. x
	 * common paramenter.
	 */
	private static PedidoVentaDetalleDAO instance;

	private PedidoVentaDetalleDAO() {
		logger.fine("created PedidoVentaDetalleDAO.");
	}

	public static PedidoVentaDetalleDAO getInstance() {
		if (instance == null) {
			instance = new PedidoVentaDetalleDAO();
		}
		return instance;
	}

	private Connection getConnection() {
		return DataSourceFacade.getStrategy().getConnection();
	}

	private Connection getConnectionCommiteable() {
		return DataSourceFacade.getStrategy().getConnectionCommiteable();
	}

	public PedidoVentaDetalle findBy(PedidoVentaDetalle x) throws DAOException, EntityNotFoundException {
		PedidoVentaDetalle r = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		try {
			conn = getConnection();
			ps = conn.prepareStatement("SELECT ID,PEDIDO_VENTA_ID,PRODUCTO_CODIGO_BARRAS,ALMACEN_ID,CANTIDAD,PRECIO_VENTA FROM PEDIDO_VENTA_DETALLE "
					+ "WHERE ID=?"
			);
			ps.setInt(1, x.getId());

			rs = ps.executeQuery();
			if (rs.next()) {
				r = new PedidoVentaDetalle();
				r.setId((Integer) rs.getObject("ID"));
				r.setPedidoVentaId((Integer) rs.getObject("PEDIDO_VENTA_ID"));
				r.setProductoCodigoBarras((String) rs.getObject("PRODUCTO_CODIGO_BARRAS"));
				r.setAlmacenId((Integer) rs.getObject("ALMACEN_ID"));
				r.setCantidad((Integer) rs.getObject("CANTIDAD"));
				r.setPrecioVenta((Double) rs.getObject("PRECIO_VENTA"));
			} else {
				throw new EntityNotFoundException("PEDIDO_VENTA_DETALLE NOT FOUND FOR ID=" + x.getId());
			}
		} catch (SQLException ex) {
			logger.log(Level.SEVERE, "SQLException:", ex);
			throw new DAOException("InQuery:" + ex.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
					ps.close();
					conn.close();
				} catch (SQLException ex) {
					logger.log(Level.SEVERE, "clossing, SQLException:" + ex.getMessage());
					throw new DAOException("Closing:" + ex.getMessage());
				}
			}
		}
		return r;
	}

	public ArrayList<PedidoVentaDetalle> findAll() throws DAOException {
		ArrayList<PedidoVentaDetalle> r = new ArrayList<PedidoVentaDetalle>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		try {
			conn = getConnection();
			ps = conn.prepareStatement("SELECT ID,PEDIDO_VENTA_ID,PRODUCTO_CODIGO_BARRAS,ALMACEN_ID,CANTIDAD,PRECIO_VENTA FROM PEDIDO_VENTA_DETALLE");

			rs = ps.executeQuery();
			while (rs.next()) {
				PedidoVentaDetalle x = new PedidoVentaDetalle();
				x.setId((Integer) rs.getObject("ID"));
				x.setPedidoVentaId((Integer) rs.getObject("PEDIDO_VENTA_ID"));
				x.setProductoCodigoBarras((String) rs.getObject("PRODUCTO_CODIGO_BARRAS"));
				x.setAlmacenId((Integer) rs.getObject("ALMACEN_ID"));
				x.setCantidad((Integer) rs.getObject("CANTIDAD"));
				x.setPrecioVenta((Double) rs.getObject("PRECIO_VENTA"));
				r.add(x);
			}
		} catch (SQLException ex) {
			logger.log(Level.SEVERE, "SQLException:", ex);
			throw new DAOException("InQuery:" + ex.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
					ps.close();
					conn.close();
				} catch (SQLException ex) {
					logger.log(Level.SEVERE, "clossing, SQLException:" + ex.getMessage());
					throw new DAOException("Closing:" + ex.getMessage());
				}
			}
		}
		return r;
	}

	;
    
    public int insert(PedidoVentaDetalle x) throws DAOException {
		PreparedStatement ps = null;
		int r = -1;
		Connection conn = null;
		try {
			conn = getConnection();
			ps = conn.prepareStatement("INSERT INTO PEDIDO_VENTA_DETALLE(PEDIDO_VENTA_ID,PRODUCTO_CODIGO_BARRAS,ALMACEN_ID,CANTIDAD,PRECIO_VENTA) "
					+ " VALUES(?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
			int ci = 1;
			ps.setObject(ci++, x.getId());
			ps.setObject(ci++, x.getPedidoVentaId());
			ps.setObject(ci++, x.getProductoCodigoBarras());
			ps.setObject(ci++, x.getAlmacenId());
			ps.setObject(ci++, x.getCantidad());
			ps.setObject(ci++, x.getPrecioVenta());

			r = ps.executeUpdate();
			ResultSet rsk = ps.getGeneratedKeys();
			if (rsk != null) {
				while (rsk.next()) {
					x.setId((Integer) rsk.getObject(1));
				}
			}
		} catch (SQLException ex) {
			logger.severe("insert:" + ex.getMessage());
			throw new DAOException("InUpdate:" + ex.getMessage());
		} finally {
			if (ps != null) {
				try {
					ps.close();
					conn.close();
				} catch (SQLException ex) {
					logger.log(Level.SEVERE, "clossing, SQLException:" + ex.getMessage());
					throw new DAOException("Closing:" + ex.getMessage());
				}
			}
		}
		return r;
	}

	public int update(PedidoVentaDetalle x) throws DAOException {
		PreparedStatement ps = null;
		int r = -1;
		Connection conn = null;
		try {
			conn = getConnection();
			ps = conn.prepareStatement("UPDATE PEDIDO_VENTA_DETALLE SET PEDIDO_VENTA_ID=?,PRODUCTO_CODIGO_BARRAS=?,ALMACEN_ID=?,CANTIDAD=?,PRECIO_VENTA=? "
					+ " WHERE ID=?");

			int ci = 1;
			ps.setObject(ci++, x.getId());
			ps.setObject(ci++, x.getPedidoVentaId());
			ps.setObject(ci++, x.getProductoCodigoBarras());
			ps.setObject(ci++, x.getAlmacenId());
			ps.setObject(ci++, x.getCantidad());
			ps.setObject(ci++, x.getPrecioVenta());
			ps.setObject(ci++, x.getId());

			r = ps.executeUpdate();
		} catch (SQLException ex) {
			logger.severe("update:" + ex.getMessage());
			throw new DAOException("InUpdate:" + ex.getMessage());
		} finally {
			if (ps != null) {
				try {
					ps.close();
					conn.close();
				} catch (SQLException ex) {
					logger.severe("update:clossing:" + ex.getMessage());
					throw new DAOException("Closing:" + ex.getMessage());
				}
			}
		}
		return r;
	}

	public int delete(PedidoVentaDetalle x) throws DAOException {
		PreparedStatement ps = null;
		int r = -1;
		Connection conn = null;
		try {
			conn = getConnection();
			ps = conn.prepareStatement("DELETE FROM PEDIDO_VENTA_DETALLE WHERE ID=?");
			ps.setObject(1, x.getId());

			r = ps.executeUpdate();
		} catch (SQLException ex) {
			logger.severe("delete:" + ex.getMessage());
			throw new DAOException("InUpdate:" + ex.getMessage());
		} finally {
			if (ps != null) {
				try {
					ps.close();
					conn.close();
				} catch (SQLException ex) {
					logger.severe("delete:clossing:" + ex.getMessage());
					throw new DAOException("Closing:" + ex.getMessage());
				}
			}
		}
		return r;
	}

}
