
package com.pmarlen.backend.model;

import java.io.Serializable;
import java.util.Set;
import java.util.MissingFormatArgumentException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;


/**
 * Class for mapping DTO Entity of Table Entrada_Salida_almacen.
 * 
 * @author Tracktopell::jpa-builder @see  https://github.com/tracktopell/UtilProjects/tree/master/jpa-builder
 * @date 2015/02/05 19:05
 */

public class EntradaSalidaAlmacen implements java.io.Serializable {
    private static final long serialVersionUID = 529312209;
    
    /**
    * id
    */
    private Integer id;
    
    /**
    * sucursal id
    */
    private int sucursalId;
    
    /**
    * usuario email
    */
    private String usuarioEmail;
    
    /**
    * tipo movimiento
    */
    private int tipoMovimiento;
    
    /**
    * fecha
    */
    private java.sql.Timestamp fecha;
    
    /**
    * comentarios
    */
    private String comentarios;
    
    /**
    * descuento aplicado
    */
    private Double descuentoAplicado;
    
    /**
    * estado
    */
    private int estado;
    
    /**
    * cfd id
    */
    private Integer cfdId;
    
    /**
    * cliente id
    */
    private Integer clienteId;
    
    /**
    * pedido venta id
    */
    private Integer pedidoVentaId;

    /** 
     * Default Constructor
     */
    public EntradaSalidaAlmacen() {
    }

    /** 
     * lazy Constructor just with IDs
     */
    public EntradaSalidaAlmacen( Integer id ) {
        this.id 	= 	id;

    }
    
    /**
     * Getters and Setters
     */
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer v) {
        this.id = v;
    }

    public int getSucursalId() {
        return this.sucursalId;
    }

    public void setSucursalId(int v) {
        this.sucursalId = v;
    }

    public String getUsuarioEmail() {
        return this.usuarioEmail;
    }

    public void setUsuarioEmail(String v) {
        this.usuarioEmail = v;
    }

    public int getTipoMovimiento() {
        return this.tipoMovimiento;
    }

    public void setTipoMovimiento(int v) {
        this.tipoMovimiento = v;
    }

    public java.sql.Timestamp getFecha() {
        return this.fecha;
    }

    public void setFecha(java.sql.Timestamp v) {
        this.fecha = v;
    }

    public String getComentarios() {
        return this.comentarios;
    }

    public void setComentarios(String v) {
        this.comentarios = v;
    }

    public Double getDescuentoAplicado() {
        return this.descuentoAplicado;
    }

    public void setDescuentoAplicado(Double v) {
        this.descuentoAplicado = v;
    }

    public int getEstado() {
        return this.estado;
    }

    public void setEstado(int v) {
        this.estado = v;
    }

    public Integer getCfdId() {
        return this.cfdId;
    }

    public void setCfdId(Integer v) {
        this.cfdId = v;
    }

    public Integer getClienteId() {
        return this.clienteId;
    }

    public void setClienteId(Integer v) {
        this.clienteId = v;
    }

    public Integer getPedidoVentaId() {
        return this.pedidoVentaId;
    }

    public void setPedidoVentaId(Integer v) {
        this.pedidoVentaId = v;
    }


    @Override
    public int hashCode() {
        int hash = 0;
		// bug ?
        hash = ( (id != null ? id.hashCode() : 0 ) );
        return hash;
    }

    public boolean equals(Object o){

        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(o instanceof EntradaSalidaAlmacen)) {
            return false;
        }

    	EntradaSalidaAlmacen other = (EntradaSalidaAlmacen ) o;
        if ( (this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }


    	return true;
    }

    @Override
    public String toString() {
        return "com.pmarlen.backend.model.EntradaSalidaAlmacen[id = "+id+ "]";
    }

	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdddHHmmss");
	private static final DecimalFormat    df  = new DecimalFormat("0.000000");
	private static final DecimalFormat    cur = new DecimalFormat("0.00");

	public String printPlainSeparated(String s){
		String ser=null;
		StringBuffer sb= new StringBuffer();

		
		// Integer
		sb.append(this.id);
		sb.append(s);
		// int
		sb.append(this.sucursalId);
		sb.append(s);
		// String
		sb.append(this.usuarioEmail);
		sb.append(s);
		// int
		sb.append(this.tipoMovimiento);
		sb.append(s);
		// java.sql.Timestamp
		sb.append(this.fecha==null?"null":sdf.format(this.fecha));
		sb.append(s);
		// String
		sb.append(this.comentarios);
		sb.append(s);
		// Double
		sb.append( df.format(this.descuentoAplicado));
		sb.append(s);
		// int
		sb.append(this.estado);
		sb.append(s);
		// Integer
		sb.append(this.cfdId);
		sb.append(s);
		// Integer
		sb.append(this.clienteId);
		sb.append(s);
		// Integer
		sb.append(this.pedidoVentaId);

		return ser;
	}

	public void scanFrom(String src, String s) throws MissingFormatArgumentException{
		String srcSpplited[] = src.split(s);
		int nf=0;
		try {			
			
			// Integer
			this.id =  Integer.parseInt(srcSpplited[nf]);
			nf++;
			// int
			this.sucursalId =  Integer.parseInt(srcSpplited[nf]);
			nf++;
			// String
			this.usuarioEmail = srcSpplited[nf].equals("null")?null:srcSpplited[nf];
			nf++;
			// int
			this.tipoMovimiento =  Integer.parseInt(srcSpplited[nf]);
			nf++;
			// java.sql.Timestamp
			this.fecha =  srcSpplited[nf].equals("null")?null:new java.sql.Timestamp(sdf.parse(srcSpplited[nf]).getTime());
			nf++;
			// String
			this.comentarios = srcSpplited[nf].equals("null")?null:srcSpplited[nf];
			nf++;
			// Double
			this.descuentoAplicado =  df.parse(srcSpplited[nf]).doubleValue();
			nf++;
			// int
			this.estado =  Integer.parseInt(srcSpplited[nf]);
			nf++;
			// Integer
			this.cfdId =  Integer.parseInt(srcSpplited[nf]);
			nf++;
			// Integer
			this.clienteId =  Integer.parseInt(srcSpplited[nf]);
			nf++;
			// Integer
			this.pedidoVentaId =  Integer.parseInt(srcSpplited[nf]);
			nf++;

		}catch(Exception e){
			throw new MissingFormatArgumentException("Exception scanning for["+nf+"] from string ->"+srcSpplited[nf]+"<-");
		}
	}

}
