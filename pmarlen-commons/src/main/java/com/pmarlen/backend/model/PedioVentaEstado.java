
package com.pmarlen.backend.model;

import java.io.Serializable;
import java.util.Set;
import java.util.MissingFormatArgumentException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;


/**
 * Class for mapping DTO Entity of Table Pedio_Venta_Estado.
 * 
 * @author Tracktopell::jpa-builder @see  https://github.com/tracktopell/UtilProjects/tree/master/jpa-builder
 * @date 2014/12/12 18:56
 */

public class PedioVentaEstado implements java.io.Serializable {
    private static final long serialVersionUID = 2033084410;
    
    /**
    * id
    */
    private Integer id;
    
    /**
    * pedido venta id
    */
    private int pedidoVentaId;
    
    /**
    * estado id
    */
    private int estadoId;
    
    /**
    * fecha
    */
    private java.util.Date fecha;
    
    /**
    * usuario email
    */
    private String usuarioEmail;
    
    /**
    * comentarios
    */
    private String comentarios;

    /** 
     * Default Constructor
     */
    public PedioVentaEstado() {
    }

    /** 
     * lazy Constructor just with IDs
     */
    public PedioVentaEstado( Integer id ) {
        this.id 	= 	id;

    }
    
    /**
     * Getters and Setters
     */
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer v) {
        this.id = v;
    }

    public int getPedidoVentaId() {
        return this.pedidoVentaId;
    }

    public void setPedidoVentaId(int v) {
        this.pedidoVentaId = v;
    }

    public int getEstadoId() {
        return this.estadoId;
    }

    public void setEstadoId(int v) {
        this.estadoId = v;
    }

    public java.util.Date getFecha() {
        return this.fecha;
    }

    public void setFecha(java.util.Date v) {
        this.fecha = v;
    }

    public String getUsuarioEmail() {
        return this.usuarioEmail;
    }

    public void setUsuarioEmail(String v) {
        this.usuarioEmail = v;
    }

    public String getComentarios() {
        return this.comentarios;
    }

    public void setComentarios(String v) {
        this.comentarios = v;
    }


    @Override
    public int hashCode() {
        int hash = 0;
		// bug ?
        hash = ( (id != null ? id.hashCode() : 0 ) );
        return hash;
    }

    public boolean equals(Object o){

        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(o instanceof PedioVentaEstado)) {
            return false;
        }

    	PedioVentaEstado other = (PedioVentaEstado ) o;
        if ( (this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }


    	return true;
    }

    @Override
    public String toString() {
        return "com.pmarlen.backend.model.PedioVentaEstado[id = "+id+ "]";
    }

	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdddHHmmss");
	private static final DecimalFormat    df  = new DecimalFormat("0.000000");
	private static final DecimalFormat    cur = new DecimalFormat("0.00");

	public String printPlainSeparated(String s){
		String ser=null;
		StringBuffer sb= new StringBuffer();

		
		// Integer
		sb.append(this.id);
		sb.append(s);
		// int
		sb.append(this.pedidoVentaId);
		sb.append(s);
		// int
		sb.append(this.estadoId);
		sb.append(s);
		// java.util.Date
		sb.append(this.fecha==null?"null":sdf.format(this.fecha));
		sb.append(s);
		// String
		sb.append(this.usuarioEmail);
		sb.append(s);
		// String
		sb.append(this.comentarios);

		return ser;
	}

	public void scanFrom(String src, String s) throws MissingFormatArgumentException{
		String srcSpplited[] = src.split(s);
		int nf=0;
		try {			
			
			// Integer
			this.id =  Integer.parseInt(srcSpplited[nf]);
			nf++;
			// int
			this.pedidoVentaId =  Integer.parseInt(srcSpplited[nf]);
			nf++;
			// int
			this.estadoId =  Integer.parseInt(srcSpplited[nf]);
			nf++;
			// java.util.Date
			this.fecha =  srcSpplited[nf].equals("null")?null:sdf.parse(srcSpplited[nf]);
			nf++;
			// String
			this.usuarioEmail = srcSpplited[nf].equals("null")?null:srcSpplited[nf];
			nf++;
			// String
			this.comentarios = srcSpplited[nf].equals("null")?null:srcSpplited[nf];
			nf++;

		}catch(Exception e){
			throw new MissingFormatArgumentException("Exception scanning for["+nf+"] from string ->"+srcSpplited[nf]+"<-");
		}
	}

}
