
package com.pmarlen.backend.model;

import java.io.Serializable;
import java.util.Set;
import java.util.MissingFormatArgumentException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;


/**
 * Class for mapping DTO Entity of Table Pedido_Venta.
 * 
 * @author Tracktopell::jpa-builder @see  https://github.com/tracktopell/UtilProjects/tree/master/jpa-builder
 * @date 2015/01/30 18:24
 */

public class PedidoVenta implements java.io.Serializable {
    private static final long serialVersionUID = 1122088620;
    
    /**
    * id
    */
    private Integer id;
    
    /**
    * sucursal id
    */
    private int sucursalId;
    
    /**
    * estado id
    */
    private int estadoId;
    
    /**
    * fecha creo
    */
    private java.sql.Timestamp fechaCreo;
    
    /**
    * usuario email creo
    */
    private String usuarioEmailCreo;
    
    /**
    * cliente id
    */
    private Integer clienteId;
    
    /**
    * forma de pago id
    */
    private Integer formaDePagoId;
    
    /**
    * metodo de pago id
    */
    private Integer metodoDePagoId;
    
    /**
    * factoriva
    */
    private double factoriva;
    
    /**
    * comentarios
    */
    private String comentarios;
    
    /**
    * cfd venta id
    */
    private Integer cfdVentaId;
    
    /**
    * numero ticket
    */
    private String numeroTicket;
    
    /**
    * caja
    */
    private Integer caja;
    
    /**
    * importe recibido
    */
    private Double importeRecibido;
    
    /**
    * aprobacion visa mastercard
    */
    private String aprobacionVisaMastercard;
    
    /**
    * porcentaje descuento calculado
    */
    private Integer porcentajeDescuentoCalculado;
    
    /**
    * porcentaje descuento extra
    */
    private Integer porcentajeDescuentoExtra;
    
    /**
    * condiciones de pago
    */
    private String condicionesDePago;
    
    /**
    * num de cuenta
    */
    private String numDeCuenta;

    /** 
     * Default Constructor
     */
    public PedidoVenta() {
    }

    /** 
     * lazy Constructor just with IDs
     */
    public PedidoVenta( Integer id ) {
        this.id 	= 	id;

    }
    
    /**
     * Getters and Setters
     */
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer v) {
        this.id = v;
    }

    public int getSucursalId() {
        return this.sucursalId;
    }

    public void setSucursalId(int v) {
        this.sucursalId = v;
    }

    public int getEstadoId() {
        return this.estadoId;
    }

    public void setEstadoId(int v) {
        this.estadoId = v;
    }

    public java.sql.Timestamp getFechaCreo() {
        return this.fechaCreo;
    }

    public void setFechaCreo(java.sql.Timestamp v) {
        this.fechaCreo = v;
    }

    public String getUsuarioEmailCreo() {
        return this.usuarioEmailCreo;
    }

    public void setUsuarioEmailCreo(String v) {
        this.usuarioEmailCreo = v;
    }

    public Integer getClienteId() {
        return this.clienteId;
    }

    public void setClienteId(Integer v) {
        this.clienteId = v;
    }

    public Integer getFormaDePagoId() {
        return this.formaDePagoId;
    }

    public void setFormaDePagoId(Integer v) {
        this.formaDePagoId = v;
    }

    public Integer getMetodoDePagoId() {
        return this.metodoDePagoId;
    }

    public void setMetodoDePagoId(Integer v) {
        this.metodoDePagoId = v;
    }

    public double getFactoriva() {
        return this.factoriva;
    }

    public void setFactoriva(double v) {
        this.factoriva = v;
    }

    public String getComentarios() {
        return this.comentarios;
    }

    public void setComentarios(String v) {
        this.comentarios = v;
    }

    public Integer getCfdVentaId() {
        return this.cfdVentaId;
    }

    public void setCfdVentaId(Integer v) {
        this.cfdVentaId = v;
    }

    public String getNumeroTicket() {
        return this.numeroTicket;
    }

    public void setNumeroTicket(String v) {
        this.numeroTicket = v;
    }

    public Integer getCaja() {
        return this.caja;
    }

    public void setCaja(Integer v) {
        this.caja = v;
    }

    public Double getImporteRecibido() {
        return this.importeRecibido;
    }

    public void setImporteRecibido(Double v) {
        this.importeRecibido = v;
    }

    public String getAprobacionVisaMastercard() {
        return this.aprobacionVisaMastercard;
    }

    public void setAprobacionVisaMastercard(String v) {
        this.aprobacionVisaMastercard = v;
    }

    public Integer getPorcentajeDescuentoCalculado() {
        return this.porcentajeDescuentoCalculado;
    }

    public void setPorcentajeDescuentoCalculado(Integer v) {
        this.porcentajeDescuentoCalculado = v;
    }

    public Integer getPorcentajeDescuentoExtra() {
        return this.porcentajeDescuentoExtra;
    }

    public void setPorcentajeDescuentoExtra(Integer v) {
        this.porcentajeDescuentoExtra = v;
    }

    public String getCondicionesDePago() {
        return this.condicionesDePago;
    }

    public void setCondicionesDePago(String v) {
        this.condicionesDePago = v;
    }

    public String getNumDeCuenta() {
        return this.numDeCuenta;
    }

    public void setNumDeCuenta(String v) {
        this.numDeCuenta = v;
    }


    @Override
    public int hashCode() {
        int hash = 0;
		// bug ?
        hash = ( (id != null ? id.hashCode() : 0 ) );
        return hash;
    }

    public boolean equals(Object o){

        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(o instanceof PedidoVenta)) {
            return false;
        }

    	PedidoVenta other = (PedidoVenta ) o;
        if ( (this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }


    	return true;
    }

    @Override
    public String toString() {
        return "com.pmarlen.backend.model.PedidoVenta[id = "+id+ "]";
    }

	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdddHHmmss");
	private static final DecimalFormat    df  = new DecimalFormat("0.000000");
	private static final DecimalFormat    cur = new DecimalFormat("0.00");

	public String printPlainSeparated(String s){
		String ser=null;
		StringBuffer sb= new StringBuffer();

		
		// Integer
		sb.append(this.id);
		sb.append(s);
		// int
		sb.append(this.sucursalId);
		sb.append(s);
		// int
		sb.append(this.estadoId);
		sb.append(s);
		// java.sql.Timestamp
		sb.append(this.fechaCreo==null?"null":sdf.format(this.fechaCreo));
		sb.append(s);
		// String
		sb.append(this.usuarioEmailCreo);
		sb.append(s);
		// Integer
		sb.append(this.clienteId);
		sb.append(s);
		// Integer
		sb.append(this.formaDePagoId);
		sb.append(s);
		// Integer
		sb.append(this.metodoDePagoId);
		sb.append(s);
		// double
		sb.append( df.format(this.factoriva));
		sb.append(s);
		// String
		sb.append(this.comentarios);
		sb.append(s);
		// Integer
		sb.append(this.cfdVentaId);
		sb.append(s);
		// String
		sb.append(this.numeroTicket);
		sb.append(s);
		// Integer
		sb.append(this.caja);
		sb.append(s);
		// Double
		sb.append( df.format(this.importeRecibido));
		sb.append(s);
		// String
		sb.append(this.aprobacionVisaMastercard);
		sb.append(s);
		// Integer
		sb.append(this.porcentajeDescuentoCalculado);
		sb.append(s);
		// Integer
		sb.append(this.porcentajeDescuentoExtra);
		sb.append(s);
		// String
		sb.append(this.condicionesDePago);
		sb.append(s);
		// String
		sb.append(this.numDeCuenta);

		return ser;
	}

	public void scanFrom(String src, String s) throws MissingFormatArgumentException{
		String srcSpplited[] = src.split(s);
		int nf=0;
		try {			
			
			// Integer
			this.id =  Integer.parseInt(srcSpplited[nf]);
			nf++;
			// int
			this.sucursalId =  Integer.parseInt(srcSpplited[nf]);
			nf++;
			// int
			this.estadoId =  Integer.parseInt(srcSpplited[nf]);
			nf++;
			// java.sql.Timestamp
			this.fechaCreo =  srcSpplited[nf].equals("null")?null:new java.sql.Timestamp(sdf.parse(srcSpplited[nf]).getTime());
			nf++;
			// String
			this.usuarioEmailCreo = srcSpplited[nf].equals("null")?null:srcSpplited[nf];
			nf++;
			// Integer
			this.clienteId =  Integer.parseInt(srcSpplited[nf]);
			nf++;
			// Integer
			this.formaDePagoId =  Integer.parseInt(srcSpplited[nf]);
			nf++;
			// Integer
			this.metodoDePagoId =  Integer.parseInt(srcSpplited[nf]);
			nf++;
			// double
			this.factoriva =  df.parse(srcSpplited[nf]).doubleValue();
			nf++;
			// String
			this.comentarios = srcSpplited[nf].equals("null")?null:srcSpplited[nf];
			nf++;
			// Integer
			this.cfdVentaId =  Integer.parseInt(srcSpplited[nf]);
			nf++;
			// String
			this.numeroTicket = srcSpplited[nf].equals("null")?null:srcSpplited[nf];
			nf++;
			// Integer
			this.caja =  Integer.parseInt(srcSpplited[nf]);
			nf++;
			// Double
			this.importeRecibido =  df.parse(srcSpplited[nf]).doubleValue();
			nf++;
			// String
			this.aprobacionVisaMastercard = srcSpplited[nf].equals("null")?null:srcSpplited[nf];
			nf++;
			// Integer
			this.porcentajeDescuentoCalculado =  Integer.parseInt(srcSpplited[nf]);
			nf++;
			// Integer
			this.porcentajeDescuentoExtra =  Integer.parseInt(srcSpplited[nf]);
			nf++;
			// String
			this.condicionesDePago = srcSpplited[nf].equals("null")?null:srcSpplited[nf];
			nf++;
			// String
			this.numDeCuenta = srcSpplited[nf].equals("null")?null:srcSpplited[nf];
			nf++;

		}catch(Exception e){
			throw new MissingFormatArgumentException("Exception scanning for["+nf+"] from string ->"+srcSpplited[nf]+"<-");
		}
	}

}
